
package command;

import ejb.model.singleton.LogSingleton;
import encryptmd5.MD5;
import entities.AppUser;
import sessionBeanFacades.AppUserFacade;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Register extends FrontCommand{
    
    private LogSingleton ls;
    private AppUserFacade userFacade;
    private MessageDigest md;
    
    @Override
    public void process() {    
        ls = getLogBean();
        ls.addLogMessage("--> REGISTER COMMAND");
        
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        userFacade = getUserFacade();        
        if(registerHelper())
            forward("/index.jsp");
        else
            forward("/JSP-Pages/Register.jsp");
    }

    private boolean registerHelper() {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");        
        String passwordConfirm = request.getParameter("passwordConfirm");        
        
        if (password.equals(passwordConfirm)) {
            AppUser user = new AppUser(name, surname, email, username, MD5.encrypt(password));            
            userFacade.create(user);
            return true;
        }        
        return false;
    }
}
