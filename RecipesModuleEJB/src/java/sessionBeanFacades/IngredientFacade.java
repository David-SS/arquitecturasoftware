
package sessionBeanFacades;

import entities.Ingredient;
import entities.Recipe;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class IngredientFacade extends AbstractFacade<Ingredient> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IngredientFacade() {
        super(Ingredient.class);
    }
    
    public List<Ingredient> getRecipeIngredientsById (Recipe recipe) {
        clearCache();
        //return (List)recipe.getIngredientCollection();
        
        Query recipeIngredientsById = em.createNamedQuery("Ingredient.findAllOrder");
        recipeIngredientsById.setParameter("id",recipe);
        List<Ingredient> ingredientList = recipeIngredientsById.getResultList();
        if (ingredientList.isEmpty())
            return null;
        else 
            return ingredientList;
    }
    
}
