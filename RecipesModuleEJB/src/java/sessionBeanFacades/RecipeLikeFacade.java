
package sessionBeanFacades;

import entities.RecipeLike;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class RecipeLikeFacade extends AbstractFacade<RecipeLike> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RecipeLikeFacade() {
        super(RecipeLike.class);
    }
    
}
