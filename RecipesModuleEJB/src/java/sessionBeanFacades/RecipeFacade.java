
package sessionBeanFacades;

import entities.AppUser;
import entities.Recipe;
import entities.Recipe_;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


@Stateless
public class RecipeFacade extends AbstractFacade<Recipe> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RecipeFacade() {
        super(Recipe.class);
    }
    
    public List<Recipe> getAllUserRecipe (AppUser userId) {
        clearCache();
        Query userRecipesQuery = em.createNamedQuery("Recipe.findAllUserRecipes");        
        userRecipesQuery.setParameter("userId",userId);
        List<Recipe> userRecipeList = userRecipesQuery.getResultList();    
        if (userRecipeList.isEmpty())
            return null;
        else 
            return userRecipeList;
    }
    
    public Recipe getRecipeById (int id) {
        clearCache();
        Query recipeById = em.createNamedQuery("Recipe.findById");
        recipeById.setParameter("id",id);
        List<Recipe> recipeList = recipeById.getResultList();
        if (recipeList.isEmpty())
            return null;
        else 
            return recipeList.get(0);
    }
    
   public Recipe createRecipe(String name, String description, 
            int minutes, String difficulty, int private1, Date lastMod, 
            String type, String imageUri, AppUser userId) {
        clearCache();
        Query insertRecipe = em.createNativeQuery("INSERT INTO Recipe "
                + "(name,description,minutes,difficulty,private,last_mod,type,image_uri,user_id) "
                + "VALUES (?,?,?,?,?,?,?,?,?)");
        insertRecipe.setParameter("1",name);
        insertRecipe.setParameter("2",description);
        insertRecipe.setParameter("3",minutes);
        insertRecipe.setParameter("4",difficulty);
        insertRecipe.setParameter("5",private1);
        insertRecipe.setParameter("6",lastMod);
        insertRecipe.setParameter("7",type);
        insertRecipe.setParameter("8",imageUri);
        insertRecipe.setParameter("9",userId.getId());
        insertRecipe.executeUpdate();
        
        return getLastCreatedRecipe();
   }    
   private Recipe getLastCreatedRecipe () {
        clearCache();
        Query recipeById = em.createNamedQuery("Recipe.findLastCreatedRecipe");
        List<Recipe> recipeList = recipeById.getResultList();
        if (recipeList.isEmpty())
            return null;
        else 
            return recipeList.get(0);
    }
    
    public int deleteRecipeById (int id) {
        clearCache();
        Query deleteRecipe = em.createQuery("DELETE FROM Recipe r WHERE r.id = :id");
        deleteRecipe.setParameter("id",id);
        return deleteRecipe.executeUpdate();
    }
    
    public int updateRecipe (int id, String name, String description, 
            int minutes, String difficulty, int private1, Date lastMod, 
            String type, String imageUri) {
        clearCache();
        Query updateRecipe = em.createQuery(
                "UPDATE Recipe r "
                        + "SET r.name = :name,"
                        + "r.description = :description,"
                        + "r.minutes = :minutes,"
                        + "r.difficulty = :difficulty,"
                        + "r.private1 = :private1,"
                        + "r.lastMod = :lastMod,"
                        + "r.type = :type,"
                        + "r.imageUri = :imageUri"
                        + " WHERE r.id= :id");
        updateRecipe.setParameter("name",name);
        updateRecipe.setParameter("description",description);
        updateRecipe.setParameter("minutes",minutes);
        updateRecipe.setParameter("difficulty",difficulty);
        updateRecipe.setParameter("private1",private1);
        updateRecipe.setParameter("lastMod",lastMod);
        updateRecipe.setParameter("type",type);
        updateRecipe.setParameter("imageUri",imageUri);
        updateRecipe.setParameter("id",id);
        return updateRecipe.executeUpdate();
    }
    
    
    //--------------CATALOG NORMAL -------------
    public List<Recipe> getRecipeCatalogFiltered(String filter) {
        clearCache();
        Query catalogRecipesQuery = em.createNamedQuery("Recipe.findCatalogRecipes"); 
        catalogRecipesQuery.setParameter("name","%"+filter+"%");
        List<Recipe> catalogRecipeList = catalogRecipesQuery.getResultList();    
        if (catalogRecipeList.isEmpty())
            return null;
        else 
            return catalogRecipeList;
    }
    
    //--------------CATALOG CRITERIAAPI-------------
    public List<Recipe> getRecipeCatalogFilteredCriteria(String filter) {
        clearCache();   
        
        //Init
        CriteriaBuilder cb = em.getCriteriaBuilder();
        
        //Result type
        CriteriaQuery<Recipe> cq = cb.createQuery(Recipe.class);
        
        //From (entity type where we search)
        Root<Recipe> recipes = cq.from(Recipe.class);
        
        //ORDER BY NAME
        cq.orderBy(cb.asc(recipes.get(Recipe_.name)));
        
        //Predicates
        Predicate p1 = cb.equal(recipes.get(Recipe_.private1),0);
        Predicate p2 = cb.like(recipes.get(Recipe_.name),"%"+filter+"%");
        Predicate p1_and_p2 = cb.and(p1,p2);
        
        //Aplying where clause with predicate
        cq.where(p1_and_p2);
        
        //Doing select and query
        cq.select(recipes);
        TypedQuery<Recipe> q = em.createQuery(cq);
        
        List<Recipe> catalogRecipeList = q.getResultList();
        if (catalogRecipeList.isEmpty())
            return null;
        else 
            return catalogRecipeList;      
    }
    
}
