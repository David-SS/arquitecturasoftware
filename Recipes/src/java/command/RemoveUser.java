
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import sessionBeanFacades.AppUserFacade;

public class RemoveUser extends FrontCommand{
    
    private LogSingleton ls;
    private AppUserFacade userFacade;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> REMOVE USER COMMAND");
        
        userFacade = getUserFacade();
        deleteRecipe();
        forward("/index.jsp");
    }

    private void deleteRecipe() {
        AppUser user = (AppUser)session.getAttribute("user");
        userFacade.deleteUserById(user.getId());
    }    
}
