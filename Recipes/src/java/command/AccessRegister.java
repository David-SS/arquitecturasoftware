
package command;

import ejb.model.singleton.LogSingleton;

public class AccessRegister extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("--> ACCESS REGISTER COMMAND");
        forward("/JSP-Pages/Register.jsp");
    }
}
