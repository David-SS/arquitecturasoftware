
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "recipe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recipe.findAll", query = "SELECT r FROM Recipe r")
    , @NamedQuery(name = "Recipe.findById", query = "SELECT r FROM Recipe r WHERE r.id = :id")
    , @NamedQuery(name = "Recipe.findByName", query = "SELECT r FROM Recipe r WHERE r.name = :name")
    , @NamedQuery(name = "Recipe.findByDescription", query = "SELECT r FROM Recipe r WHERE r.description = :description")
    , @NamedQuery(name = "Recipe.findByImageUri", query = "SELECT r FROM Recipe r WHERE r.imageUri = :imageUri")
    , @NamedQuery(name = "Recipe.findByMinutes", query = "SELECT r FROM Recipe r WHERE r.minutes = :minutes")
    , @NamedQuery(name = "Recipe.findByDifficulty", query = "SELECT r FROM Recipe r WHERE r.difficulty = :difficulty")
    , @NamedQuery(name = "Recipe.findByPrivate1", query = "SELECT r FROM Recipe r WHERE r.private1 = :private1")
    , @NamedQuery(name = "Recipe.findByLastMod", query = "SELECT r FROM Recipe r WHERE r.lastMod = :lastMod")
    , @NamedQuery(name = "Recipe.findByType", query = "SELECT r FROM Recipe r WHERE r.type = :type")
    , @NamedQuery(name = "Recipe.findAllUserRecipes", query = "SELECT r FROM Recipe r WHERE r.userId = :userId")
    , @NamedQuery(name = "Recipe.findCatalogRecipes", query = "SELECT r FROM Recipe r WHERE r.private1 = 0 AND r.name LIKE :name ORDER BY r.name")
    , @NamedQuery(name = "Recipe.findLastCreatedRecipe", query = "SELECT r FROM Recipe r ORDER BY r.id DESC")})
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 400)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "image_uri")
    private String imageUri;
    @Basic(optional = false)
    @NotNull
    @Column(name = "minutes")
    private int minutes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "difficulty")
    private String difficulty;
    @Basic(optional = false)
    @NotNull
    @Column(name = "private")
    private int private1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_mod")
    @Temporal(TemporalType.DATE)
    private Date lastMod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "type")
    private String type;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<Ingredient> ingredientCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<FavoriteRecipe> favoriteRecipeCollection;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AppUser userId;
    @OneToMany(mappedBy = "recipeId")
    private Collection<Step> stepCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<RecipeLike> recipeLikeCollection;

    public Recipe() {
    }

    public Recipe(Integer id) {
        this.id = id;
    }

    public Recipe(Integer id, String name, String imageUri, int minutes, String difficulty, int private1, Date lastMod, String type) {
        this.id = id;
        this.name = name;
        this.imageUri = imageUri;
        this.minutes = minutes;
        this.difficulty = difficulty;
        this.private1 = private1;
        this.lastMod = lastMod;
        this.type = type;
    }

    public Recipe(Integer id, String name, String description, int minutes, String difficulty, int private1, Date lastMod, String type, String imageUri) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageUri = imageUri;
        this.minutes = minutes;
        this.difficulty = difficulty;
        this.private1 = private1;
        this.lastMod = lastMod;
        this.type = type;
    }

    public Recipe(String name, String description, int minutes, String difficulty, int private1, Date lastMod, String type, String imageUri) {
        this.name = name;
        this.description = description;
        this.imageUri = imageUri;
        this.minutes = minutes;
        this.difficulty = difficulty;
        this.private1 = private1;
        this.lastMod = lastMod;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public int getPrivate1() {
        return private1;
    }

    public void setPrivate1(int private1) {
        this.private1 = private1;
    }

    public Date getLastMod() {
        return lastMod;
    }

    public void setLastMod(Date lastMod) {
        this.lastMod = lastMod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public Collection<Ingredient> getIngredientCollection() {
        return ingredientCollection;
    }

    public void setIngredientCollection(Collection<Ingredient> ingredientCollection) {
        this.ingredientCollection = ingredientCollection;
    }

    @XmlTransient
    public Collection<FavoriteRecipe> getFavoriteRecipeCollection() {
        return favoriteRecipeCollection;
    }

    public void setFavoriteRecipeCollection(Collection<FavoriteRecipe> favoriteRecipeCollection) {
        this.favoriteRecipeCollection = favoriteRecipeCollection;
    }

    public AppUser getUserId() {
        return userId;
    }

    public void setUserId(AppUser userId) {
        this.userId = userId;
    }

    @XmlTransient
    public Collection<Step> getStepCollection() {
        return stepCollection;
    }

    public void setStepCollection(Collection<Step> stepCollection) {
        this.stepCollection = stepCollection;
    }

    @XmlTransient
    public Collection<RecipeLike> getRecipeLikeCollection() {
        return recipeLikeCollection;
    }

    public void setRecipeLikeCollection(Collection<RecipeLike> recipeLikeCollection) {
        this.recipeLikeCollection = recipeLikeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recipe)) {
            return false;
        }
        Recipe other = (Recipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Recipe[ id=" + id + " ]";
    }
    
}
