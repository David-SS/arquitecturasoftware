
package command;

import ejb.model.singleton.LogSingleton;

public class RecipeList  extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> RECIPE LIST COMMAND");
        forward("/JSP-Pages/RecipeList.jsp");
    }
}
