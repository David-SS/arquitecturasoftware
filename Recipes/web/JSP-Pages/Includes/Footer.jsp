<footer>
    <section id="footer">
        <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <h5>Colaboradores</h5>
                    <ul class="list-unstyled quick-links">
                            <li><a><i class="fa fa-angle-double-right"></i>David Su�rez Su�rez</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>928 82 99 28</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>600 00 66 00</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>alguieninteresante@example.es</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <h5>Agradecimientos</h5>
                    <ul class="list-unstyled quick-links">
                            <li><a><i class="fa fa-angle-double-right"></i>Creadores de HTML</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>Creadores de CSS</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>Creadores de Bootstrap</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>Creadores de Java</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <h5>Recursos externos</h5>
                    <ul class="list-unstyled quick-links">
                            <li><a><i class="fa fa-angle-double-right"></i>MiRecetario.es</a></li>
                            <li><a><i class="fa fa-angle-double-right"></i>Comparecetas.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                            <ul class="list-unstyled list-inline social text-center">
                                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                    </div>
                    </hr>
            </div>	
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                            <p>Reg. Spain MakeYourRecipes SL </p>
                            <p class="h6">&copy All fiction right Reversed.<a class="text-green ml-2" href="https://www.sunlimetech.com" target="_blank">MakeYourRecipes.com</a></p>
                    </div>
                    </hr>
            </div>	
        </div>
    </section>
</footer>        
    

