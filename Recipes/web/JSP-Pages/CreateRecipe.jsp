
<%@page import="entities.Recipe"%>
<%@page import="java.util.List"%>
<%@page import="sessionBeanFacades.RecipeFacade"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP CREATE RECIPE");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("CreateRecipe.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Crear Receta</title>
    </head>
    <body>

        <%@include file="Includes/Header.jsp" %>

        <% 
            RecipeFacade recipeFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/RecipeFacade!sessionBeanFacades.RecipeFacade");
            List<Recipe> myRecipes = recipeFacade.getAllUserRecipe(sessionUser);  
        %>

        <main>
            <h2 class="ActualPage">
                <a id="backButton" href="FrontController?command=RecipeList">
                    <img src="Images/ForWebPage/back.png" width="40" height="auto">
                </a>
                Crear Receta
            </h2>
            <div class="contentMargin">
                <% if (myRecipes == null) { %>
                    <form  action="FrontController" onsubmit="return createForm(0);">
                <% } else { %>
                    <form  action="FrontController" onsubmit="return createForm( <%= myRecipes.size() %> );">
                <% } %>
                
                <div class="row">
                    
                    <div class="inside_elements_margin">
                        <img id="inputImageItem" src="Images/default.png" width="600" height="auto">
                        <div class="form-group">
                            <input id="inputFileImage" name="inputFileImage" type="file" onchange="getImagePath();">
                        </div>
                    </div>
                    
                    <div class="inside_elements_margin">
                        <div class="form-group">
                            <label for="pwd"> Nombre: </label> 
                            <input placeholder="Papas Arrugadas con Mojo" class="form-control" type="text" name="name" required>
                        </div>

                        <div class="form-group">
                            <label for="pwd"> Descripción: </label> 
                            <textarea class="form-control" rows="4" cols="50" name="description" 
                                placeholder="Receta canaria muy rica, pruebala"></textarea>
                        </div>                  

                        <div id="ingredients_group" class="form-group">
                            <label for="pwd"> Ingredientes: </label>
                            <div class="form-control">
                                Ingrediente 1:
                                <input placeholder="Papas" type="text" id="ingredient_1" name="ingredient_1" required>
                                Cantidad: <input placeholder="10" type="text" id="ingredient_quantity_1" name="ingredient_quantity_1" required/>
                                Calorías: <input placeholder="10" type="number" id="ingredient_calories_1" name="ingredient_calories_1" required/>
                                Tipo: <select id="ingredient_type_1" name="ingredient_type_1">
                                    <option value="CARNE">Carne</option> 
                                    <option value="PESCADO">Pescado</option> 
                                    <option value="FRUTA">Fruta</option>
                                    <option value="VERDURA">Verdura</option> 
                                    <option value="TUBERCULO">Tuberculo</option> 
                                    <option value="CEREAL">Cereal</option> 
                                    <option value="LEGUMBRE">Legumbre</option>
                                    <option value="LACTEO">Lacteo</option>  
                                    <option value="OTRO">Otro</option> 
                                </select>
                                <input type="button" id="add_kid\(\)_1" onclick="addkid()" value="+" />
                            </div> 
                        </div>

                        <div class="form-group">
                            <label for="pwd"> Pasos: </label>
                            <textarea class="form-control" rows="4" cols="50" name="steps"
                                placeholder="Compra los ingredientes, Prepara los materiales, Sigue los pasos, A comer [Formato: Paso, Paso, Paso]" required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="pwd">Tiempo de preparación (minutos):</label> 
                            <input placeholder="12" class="form-control" type="number" name="minutes" required>
                        </div>

                        <div class="row">
                            <div class="radio-group inside_elements_margin">
                                <label> Dificultad: </label>
                                <div class="form-check">
                                    <input name="difficulty" value="Facil" type="radio" class="with-gap" required>
                                    <label>Fácil</label>
                                </div>
                                <div class="form-check">
                                    <input name="difficulty" value="Media" type="radio" class="with-gap">
                                    <label>Media</label>
                                </div>
                                <div class="form-check">
                                    <input name="difficulty" value="Dificil" type="radio" class="with-gap">
                                    <label>Difícil</label>
                                </div>                        
                            </div> 

                            <div class="radio-group inside_elements_margin">
                                <label> Tipo de plato: </label>
                                <div class="form-check">
                                    <input name="recipeType" value="Aperitivo" type="radio" class="with-gap" required>
                                    <label>Aperitivo</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Primer Plato" type="radio" class="with-gap" required>
                                    <label>Primer plato</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Segundo Plato" type="radio" class="with-gap">
                                    <label>Segundo plato</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Postre" type="radio" class="with-gap">
                                    <label>Postre</label>
                                </div>                        
                            </div>                                     
                        </div>
                        
                        <div class="form-group">
                            <input type="checkbox" name="private" value="true"> <label for="pwd"> Privado </label> 
                        </div>

                        <input type="hidden" name="command" value="CreateRecipe">
                        <input type="submit" class="btn btn-primary" value="Crear Receta">
                    </div>   
                    </form>                 
                </div>
            </div>
        </main>      
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>    
</html>