
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("--> JSP MODIFICATE PROFILE");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessageForPage("ModificateProfile.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title> Editar Perfil </title>
    </head>
    <body>
        
        <%@include file="Includes/Header.jsp" %>
        
        <main>
            <h2 class="ActualPage">
                Editar Perfil
            </h2>
            <div class="contentMargin">        
                <div class="row">                    
                    <div class="inside_elements_margin">
                        <img id="inputImageItem" src="Images/user.png" width="600" height="auto">
                        <div class="form-group">
                            <input id="inputFileImage" name="inputFileImage" type="file" onchange="getImagePath();">
                        </div>
                    </div>
                    
                    <form action="FrontController">
                    <div class="inside_elements_margin">
                        <div class="form-group">
                            <label for="pwd"> Nombre: </label> 
                            <input value="<%= sessionUser.getName() %>" class="form-control" type="text" name="name" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="pwd"> Apellidos: </label> 
                            <input value="<%= sessionUser.getSurname()%>" class="form-control" type="text" name="surname" required>
                        </div>     

                        <div class="form-group">
                            <label for="pwd"> Email: </label> 
                            <input value="<%= sessionUser.getEmail()%>" class="form-control" type="text" name="email" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Nombre de usuario: </label> 
                            <input value="<%= sessionUser.getUsername()%>" class="form-control" type="text" name="username" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Contraseña: </label> 
                            <input class="form-control" type="password" name="password" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Confirmación contraseña: </label> 
                            <input class="form-control" type="password" name="passwordConfirm" required>
                        </div>     

                        <input type="hidden" name="command" value="ModificateProfile">
                        <input type="submit" class="btn btn-primary" value="Registrarse">
                    </div>   
                    </form>                 
                </div>
            </div>
        </main>   
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>    
</html>


