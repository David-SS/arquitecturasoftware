
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "favorite_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FavoriteUser.findAll", query = "SELECT f FROM FavoriteUser f")
    , @NamedQuery(name = "FavoriteUser.findById", query = "SELECT f FROM FavoriteUser f WHERE f.id = :id")
    , @NamedQuery(name = "FavoriteUser.findAllFavoriteUsers", query = "SELECT f FROM FavoriteUser f WHERE f.fromUserId = :fromUserId")
    , @NamedQuery(name = "FavoriteUser.findFavoriteUser", query = "SELECT f FROM FavoriteUser f WHERE f.fromUserId = :fromUserId AND f.addedUserId = :addedUserId")})
public class FavoriteUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "added_user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AppUser addedUserId;
    @JoinColumn(name = "from_user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AppUser fromUserId;

    public FavoriteUser() {
    }

    public FavoriteUser(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AppUser getAddedUserId() {
        return addedUserId;
    }

    public void setAddedUserId(AppUser addedUserId) {
        this.addedUserId = addedUserId;
    }

    public AppUser getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(AppUser fromUserId) {
        this.fromUserId = fromUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FavoriteUser)) {
            return false;
        }
        FavoriteUser other = (FavoriteUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.FavoriteUser[ id=" + id + " ]";
    }
    
}
