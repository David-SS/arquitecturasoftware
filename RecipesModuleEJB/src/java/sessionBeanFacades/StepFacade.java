
package sessionBeanFacades;

import entities.Recipe;
import entities.Step;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class StepFacade extends AbstractFacade<Step> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StepFacade() {
        super(Step.class);
    }
    
    public List<Step> getRecipeStepsById (Recipe recipe) {
        clearCache();
        //return (List)recipe.getStepCollection();
        
        Query recipeStepsById = em.createNamedQuery("Step.findAllOrder");
        recipeStepsById.setParameter("id",recipe);
        List<Step> stepList = recipeStepsById.getResultList();
        if (stepList.isEmpty())
            return null;
        else 
            return stepList;
    }
    
}
