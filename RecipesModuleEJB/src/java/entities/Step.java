
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "step")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Step.findAll", query = "SELECT s FROM Step s")
    , @NamedQuery(name = "Step.findById", query = "SELECT s FROM Step s WHERE s.id = :id")
    , @NamedQuery(name = "Step.findByStepDesc", query = "SELECT s FROM Step s WHERE s.stepDesc = :stepDesc")
    , @NamedQuery(name = "Step.findByStepNum", query = "SELECT s FROM Step s WHERE s.stepNum = :stepNum")
    , @NamedQuery(name = "Step.findAllOrder", query = "SELECT s FROM Step s WHERE s.recipeId = :id ORDER BY s.stepNum ASC")})
public class Step implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "step_desc")
    private String stepDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "step_num")
    private int stepNum;
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @ManyToOne
    private Recipe recipeId;

    public Step() {
    }

    public Step(Integer id) {
        this.id = id;
    }

    public Step(Integer id, String stepDesc, int stepNum) {
        this.id = id;
        this.stepDesc = stepDesc;
        this.stepNum = stepNum;
    }

    public Step(String stepDesc, int stepNum) {
        this.stepDesc = stepDesc;
        this.stepNum = stepNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStepDesc() {
        return stepDesc;
    }

    public void setStepDesc(String stepDesc) {
        this.stepDesc = stepDesc;
    }

    public int getStepNum() {
        return stepNum;
    }

    public void setStepNum(int stepNum) {
        this.stepNum = stepNum;
    }

    public Recipe getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Recipe recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Step)) {
            return false;
        }
        Step other = (Step) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Step[ id=" + id + " ]";
    }
    
}
