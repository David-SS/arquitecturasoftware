
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>

<%
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("--> JSP INDEX (LOGIN)");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessageForPage("index.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <title> P�gina Principal </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="globalStyle.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    </head>
    <body>           
        <main>       
            <h1 class="text-center">D-Receta</h1>
            <div class="container loginContainer">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-md-offset-4"> 
                        <h1 class="text-center login-title">Inicio de Sesi�n</h1>
                        <div class="account-wall">
                            <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
                            <form class="form-signin" action="FrontController">
                            <input type="text" name="username" class="form-control" placeholder="Nombre de usuario o Correo" required autofocus>
                            <input type="password" name="password" class="form-control" placeholder="Contrase�a" required>
                            <input type="hidden" name="command" value="Login">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesi�n</button>
                            </form>
                        </div>
                        <a href="FrontController?command=AccessRegister" class="text-center new-account btn-secondary">Registrarse</a>
                    </div>
                </div>
            </div>            
        </main>
    </body>
</html>

