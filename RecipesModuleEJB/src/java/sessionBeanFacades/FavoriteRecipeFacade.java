
package sessionBeanFacades;

import entities.AppUser;
import entities.FavoriteRecipe;
import entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class FavoriteRecipeFacade extends AbstractFacade<FavoriteRecipe> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FavoriteRecipeFacade() {
        super(FavoriteRecipe.class);
    }
    
    public List<Recipe> getAllUserFavoriteRecipes (AppUser userId) {
        clearCache();
        List<Recipe> userFavoriteRecipeList = new ArrayList<>();
        Query favoriteListQuery = em.createNamedQuery("FavoriteRecipe.findAllFavoriteRecipes");
        favoriteListQuery.setParameter("userId",userId);
        List<FavoriteRecipe> favoriteList = favoriteListQuery.getResultList();
        for (FavoriteRecipe favorite : favoriteList)
            userFavoriteRecipeList.add(favorite.getRecipeId());        
        if (userFavoriteRecipeList.isEmpty())
            return null;
        else 
            return userFavoriteRecipeList;      
    }
    
    public FavoriteRecipe getFavoriteRecipeById (Recipe recipeId, AppUser userId) {
        clearCache();
        Query favoriteRecipeById = em.createNamedQuery("FavoriteRecipe.findFavoriteRecipe");
        favoriteRecipeById.setParameter("userId",userId);
        favoriteRecipeById.setParameter("recipeId",recipeId);
        List<FavoriteRecipe> favoriteRecipe = favoriteRecipeById.getResultList();
        if (favoriteRecipe.isEmpty())
            return null;
        else 
            return favoriteRecipe.get(0);        
    }
    
    public int deleteFavoriteRecipeById (Recipe recipeId, AppUser user) {
        clearCache();
        FavoriteRecipe favoriteRecipe = getFavoriteRecipeById(recipeId, user);
        Query deleteRecipe = em.createQuery("DELETE FROM FavoriteRecipe f WHERE f.id = :id");
        deleteRecipe.setParameter("id",favoriteRecipe.getId());
        return deleteRecipe.executeUpdate();
    } 
}
