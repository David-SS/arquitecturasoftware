
package command;

import ejb.model.singleton.LogSingleton;
import ejb.model.singleton.RecipeCatalog;
import sessionBeanFacades.RecipeFacade;

public class RemoveRecipe extends FrontCommand{
    
    private RecipeCatalog rc;
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    
    @Override
    public void process() {
        rc = getRecipeCatalogBean();
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> REMOVE RECIPE COMMAND");
        
        recipeFacade = getRecipeFacade();
        deleteRecipe();
        forward("/JSP-Pages/RecipeList.jsp");
    }

    private void deleteRecipe() {
        recipeFacade.deleteRecipeById(Integer.parseInt(request.getParameter("id")));
    }    
}
