
<%@page import="entities.Recipe"%>
<%@page import="sessionBeanFacades.FavoriteRecipeFacade"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP FAVORITE RECIPE LIST");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("FavoriteRecipeList.jsp");
%>

<!DOCTYPE html>
<%! final int elemsPerPage = 4; %>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Recetas Favoritas</title>
    </head>
    <body>
                
        <%@include file="Includes/Header.jsp" %>
        
        <% 
            FavoriteRecipeFacade favoriteRecipeFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/FavoriteRecipeFacade!sessionBeanFacades.FavoriteRecipeFacade");
            List<Recipe> recipeList = favoriteRecipeFacade.getAllUserFavoriteRecipes(sessionUser);  
        %>       

        <main>
            <h2 class="ActualPage">Recetas Favoritas</h2>
            
            <% if (recipeList == null) { %>
            <div class="contentMargin">
                <p> 
                    No has añadido ninguna receta. Ve ahora mismo al catalogo de
                    recetas y añade una... No esperes más!
                </p> 
                <a href="FrontController?command=RecipeCatalog" class="btn btn-secondary">Catálogo</a>                   
            </div>          
            
            <% } else { %>    
            
            <!-- ENCABEZADO: NAVEGACION RECETAS -->
            <div class="contentMargin thumbnail">
                <div class="col-sm-12 row">
                    <div class="pagination">
                        <a href="#noDest" onclick="previousPage('PageNum','PageContent')">&laquo;</a>
                        <%
                            int index;
                            String indexId="",onChangeId=""; 
                            for (index = 0; index < recipeList.size()/elemsPerPage; index++) {
                                indexId= "\"PageNum"+(index+1)+"\"";
                                onChangeId = "\"changePage("+"'PageNum','PageContent','"+ (index+1) + "')\"";
                                if (index != 0) out.println("<a href=\"#noDest\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                else out.println("<a href=\"#noDest\" id=" + indexId + " class=\"active\" onclick=" + onChangeId + ">" + (index+1) + "</a>");
                            }
                            if (recipeList.size()%elemsPerPage != 0) {
                                indexId= "\"PageNum"+(index+1)+"\"";
                                onChangeId = "\"changePage("+"'PageNum','PageContent','"+ (index+1) + "')\"";
                                if (index == 0) out.println("<a href=\"#noDest\" class=\"active\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                else out.println("<a href=\"#noDest\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                            }
                        %>
                        <a href="#noDest" onclick="nextPage('PageNum','PageContent')">&raquo;</a>
                    </div>
                </div>
                
                
                <!-- LISTADO DE RECETAS SEGMENTADAS POR PAGINAS -->
                <% 
                    int count = 0;
                    int elemsPerRow = elemsPerPage;
                    for (Recipe recipe : recipeList) {
                        if (count == 0) 
                            out.println("<div class=\"row\" id=\"PageContent"+((count/elemsPerRow)+1)+"\">");
                        if (count%elemsPerPage == 0 && count != 0)
                            out.println("<div style=\"display: none;\" class=\"row\" id=\"PageContent"+((count/elemsPerRow)+1)+"\">");
                %>
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <div class="card">
                            <a href="FrontController?command=RecipeDetails&id=<%= recipe.getId() %>">
                                <img class="card-img-top" src=" <%= recipe.getImageUri()%> ">
                            </a>
                            <div class="card-block">
                                <h4 class="card-title"><%= recipe.getName()%></h4>
                                <div class="meta float-left">
                                    <p><%= recipe.getLastMod()%></p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <span class="float-right">Dificultad: <%= recipe.getDifficulty()%></span>
                                <span><i class=""></i>Tiempo: <%= recipe.getMinutes()%></span>
                            </div>
                        </div>
                    </div>                        
                <%
                        count++;
                        elemsPerRow--;
                        if (elemsPerRow == 0) {
                            elemsPerRow = elemsPerPage;
                            out.println("</div>");
                        }
                    }
                    out.println("</div>");
                %>
            </div>  
                
            <% } %>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>
</html>

