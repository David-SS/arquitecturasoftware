
package command;

import ejb.model.singleton.LogSingleton;
import entities.Ingredient;
import entities.Recipe;
import entities.Step;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import sessionBeanFacades.IngredientFacade;
import sessionBeanFacades.RecipeFacade;
import sessionBeanFacades.StepFacade;

public class ModificateRecipe extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    private IngredientFacade ingredientFacade;
    private StepFacade stepFacade;
    
    @Override
    public void process() {      
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> MODIFICATE RECIPE COMMAND");
        
        recipeFacade = getRecipeFacade();
        ingredientFacade = getIngredientFacade();
        stepFacade = getStepsFacade();
        recipeHelper();    
        forward("/JSP-Pages/RecipeDetails.jsp");
    }
    
    private void recipeHelper() {
        Recipe recipeToModificate = recipeFacade.getRecipeById(Integer.parseInt(request.getParameter("id"))); 
        Recipe modificatedRecipe = persistRecipeChanges(recipeToModificate);              
        session.setAttribute("recipeDetails",modificatedRecipe);
    }

    private Recipe persistRecipeChanges(Recipe recipe) {                
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        int minutes = Integer.parseInt(request.getParameter("minutes"));
        String difficulty = request.getParameter("difficulty");
        String type = request.getParameter("recipeType");       
        String imagePath = request.getParameter("inputFileImage"); 
        if (imagePath.equals("")) imagePath = recipe.getImageUri();
        else imagePath = "Images/" + imagePath;
        int privateRecipe = 0;
        if (request.getParameter("private") != null) privateRecipe = 1;
        
        recipeFacade.updateRecipe(recipe.getId(), name, description, minutes, difficulty, privateRecipe, new Date(), type, imagePath);        
        removeIngredientAndStep(recipe);
        persistIngredientAndStep(recipe);
        
        return recipeFacade.getRecipeById(recipe.getId());        
    }
    
    private void removeIngredientAndStep(Recipe recipe) {
        for (Ingredient recipeIngredient : ingredientFacade.getRecipeIngredientsById(recipe))
            ingredientFacade.remove(recipeIngredient);
        for (Step recipeStep : stepFacade.getRecipeStepsById(recipe))
            stepFacade.remove(recipeStep);
    }
    
    private void persistIngredientAndStep(Recipe modificatedRecipe) {
        List<Ingredient> ingredients = getIngredientsList(modificatedRecipe);
        for (Ingredient ingredient : ingredients)
            ingredientFacade.create(ingredient);
        
        List<Step> steps = getStepList(modificatedRecipe);
        for (Step step : steps)
            stepFacade.create(step);
    }

    private List<Ingredient> getIngredientsList(Recipe recipe) {
        List<Ingredient> ingredientList = new ArrayList<>();
        int ingredientsCount = 1;
        boolean moreIngredients = true;
        while (moreIngredients) {
            if (request.getParameter("ingredient_"+Integer.toString(ingredientsCount)) == null) {
                moreIngredients = false;
            } else {
                String name = request.getParameter("ingredient_"+Integer.toString(ingredientsCount));
                String quantity = request.getParameter("ingredient_quantity_"+Integer.toString(ingredientsCount));
                String calories = request.getParameter("ingredient_calories_"+Integer.toString(ingredientsCount));
                String type = request.getParameter("ingredient_type_"+Integer.toString(ingredientsCount));
                Ingredient newIngredient = new Ingredient(name,quantity,calories,type,ingredientsCount);   
                newIngredient.setRecipeId(recipe);
                ingredientList.add(newIngredient);
            }
            ingredientsCount++;
        }
        return ingredientList;
    }    
    
    private List<Step> getStepList(Recipe recipe) {
        List<Step> stepList = new ArrayList<>();
        int stepCount = 1;
        for (String desc : request.getParameter("steps").split("[,.;:/|]\\s+")) {
            desc =  desc + "\n";
            Step newStep = new Step(desc, stepCount);
            newStep.setRecipeId(recipe);
            stepList.add(newStep);
            stepCount++;
        }
        return stepList;
    }
}
