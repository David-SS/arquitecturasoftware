
package command;

import ejb.model.singleton.LogSingleton;
import encryptmd5.MD5;
import entities.AppUser;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sessionBeanFacades.AppUserFacade;

public class ModificateProfile extends FrontCommand{
    
    private LogSingleton ls;
    private AppUserFacade userFacade;
    private MessageDigest md;
    
    @Override
    public void process() {    
        ls = getLogBean();
        ls.addLogMessage("--> MODIFICATE PROFILE COMMAND");
        
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        userFacade = getUserFacade();    
        modificateUserHelper();
        forward("/JSP-Pages/HomePage.jsp");
    }

    private void modificateUserHelper() {
        AppUser user = (AppUser) session.getAttribute("user");
        
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");        
        String passwordConfirm = request.getParameter("passwordConfirm");        
        
        if (password.equals(passwordConfirm)) {
            userFacade.updateUser(user.getId(),name, surname, email, username, MD5.encrypt(password));
            AppUser modificatedUser = userFacade.getLogedUser(username);
            session.setAttribute("user", modificatedUser);
        }
    }
}
