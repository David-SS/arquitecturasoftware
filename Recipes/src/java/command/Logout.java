
package command;

import ejb.model.singleton.StatisticsSingleton;
import ejb.model.singleton.LogSingleton;

public class Logout extends FrontCommand{
    
    private LogSingleton ls;
    private StatisticsSingleton statistics;
    
    @Override
    public void process() {    
        ls = getLogBean();        
        ls.addLogMessage("--> LOGOUT COMMAND");
        statistics = getStatisticsBean();
        statistics.removeUser();
              
        session.removeAttribute("user");
        forward("/index.jsp");
    }
}
