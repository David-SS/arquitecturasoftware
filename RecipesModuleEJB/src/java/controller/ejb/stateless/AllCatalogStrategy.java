
package controller.ejb.stateless;

import ejb.model.singleton.LogSingleton;
import entities.Recipe;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import strategy.FilterAndOrderStrategy;

@Stateless
@LocalBean
public class AllCatalogStrategy extends FilterAndOrderStrategy {

    private LogSingleton ls;
    public AllCatalogStrategy() {
        try {
            this.ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(AllCatalogStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Recipe> getFilteredResult(List<Recipe> recipeCatalog) {
        ls.addLogMessage("AllCatalogStrategy:getFilteredResult");
        return recipeCatalog;
    }
        
    @PostConstruct
    void postConstruct() {
        ls.addLogMessage("AllCatalogStrategy:POSTCONTRUCT");
    }    
    @PreDestroy
    void preDestroy() {
        ls.addLogMessage("AllCatalogStrategy:PREDESTROY");
    }   
}
