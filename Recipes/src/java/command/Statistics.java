
package command;

import ejb.model.singleton.LogSingleton;

public class Statistics extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> STATISTICS COMMAND");
        forward("/JSP-Pages/Statistics.jsp");
    }
}
