
package command;

import ejb.model.singleton.LogSingleton;

public class Log extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> LOG COMMAND");
        forward("/JSP-Pages/Log.jsp");
    }
}
