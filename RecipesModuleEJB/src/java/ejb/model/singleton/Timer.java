
package ejb.model.singleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.naming.InitialContext;
import javax.naming.NamingException;
@Singleton
@LocalBean
public class Timer {

    private File log;
    private String previousLogMessage;
    LogSingleton ls;

    @PostConstruct
    void init() {
        this.log = new File("D:\\David\\Documentos\\Estudios\\UNIVERSIDAD\\Año 4\\Segundo Semestre\\AS\\Practicas\\RecipeProjectAS\\logs\\log.txt");
        this.previousLogMessage = "";
        try {
            ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Lock(LockType.WRITE)
    @Schedule (second="*/5", minute="*", hour="*")    
    public void saveUnused() {
        //System.out.println("SAVING UNUSED BY USER");
        if (previousLogMessage.equals(getStrings(ls.getLogMessages())))
            ls.addLogMessage("*** ADVERTENCIA *** NINGUNA INTERACCIÓN EN EL SISTEMA (OCIOSO)\r\n \r\n");
    }    
    
    @Lock(LockType.WRITE)
    //@Schedule (second="*/5", minute="*", hour="*")   
    public void saveToDisk() {
        //System.out.println("SAVE");   
        this.previousLogMessage = getStrings(ls.getLogMessages());
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(this.log,false));
            bw.write(getStrings(ls.getLogMessages()));
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @Lock(LockType.READ)
    private String getStrings(List <String> logMessages){
        String messages = "";
        for (String logMessage : logMessages) {
            messages += logMessage + "\r\n \r\n";
        }
        return messages;
    }
    
}
