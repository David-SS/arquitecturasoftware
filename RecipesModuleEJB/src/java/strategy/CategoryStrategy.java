
package strategy;

import entities.Recipe;
import java.util.List;

public abstract class CategoryStrategy {    
    public abstract List<Recipe> getCategoryRecipes(List<Recipe> recipeCatalog);
}
