
package command;

import ejb.model.singleton.LogSingleton;

public class HomePage extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("--> HOME PAGE COMMAND");
        forward("/JSP-Pages/HomePage.jsp");
    }
}
