
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import entities.Recipe;
import sessionBeanFacades.FavoriteRecipeFacade;
import sessionBeanFacades.RecipeFacade;

public class FavoriteRecipeDelete extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    private FavoriteRecipeFacade favoriteRecipeFacade;
    
    @Override
    public void process() {
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> FAVORITE RECIPE DELETE COMMAND");
        
        recipeFacade = getRecipeFacade();
        favoriteRecipeFacade = getFavoriteRecipeFacade();        
        deleteFromFavorites();
        forward("/JSP-Pages/FavoriteRecipeList.jsp");
    }

    
    private void deleteFromFavorites() {
        Recipe favoriteRecipe = recipeFacade.getRecipeById(Integer.parseInt(request.getParameter("id")));
        favoriteRecipeFacade.deleteFavoriteRecipeById(favoriteRecipe, (AppUser)session.getAttribute("user"));
    }
}
