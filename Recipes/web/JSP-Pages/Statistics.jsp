
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="java.util.Map"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton"); 
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP STATISTICS");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("Statistics.jsp");
%>  

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Estadísticas</title>
    </head>
    <body>      
        
        <%@include file="Includes/Header.jsp" %>
          
        <main>
            <h2 class="ActualPage">Estadísticas</h2>
            <div class="contentMargin">
                <h5>User Access: </h5>
                <p>USUARIOS LOGUEADOS = <%= statistics.getLogedUser() %></p>
                <h5>User Statistics: </h5>
                <% for (Map.Entry<String, Integer> statistic : statistics.getStatistics().entrySet()) { %>
                    <p> <%= statistic.getKey() + " = " + statistic.getValue() %> </p>   
                <% } %>
                <h5>Page Statistics: </h5>
                <% for (Map.Entry<String, Integer> statistic : statistics.getPageStatistics().entrySet()) { %>
                    <p> <%= statistic.getKey() + " = " + statistic.getValue() %> </p>   
                <% } %>
            </div>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>
</html>
