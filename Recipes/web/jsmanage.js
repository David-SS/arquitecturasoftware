
function changePage(pagIndexString,pageContentString,index) {
    //Get current active page to unable it.
    oldIndex = getIndex();
    oldNumPage = document.getElementById(pagIndexString+oldIndex[2]);
    oldContentPage = document.getElementById(pageContentString+oldIndex[2]);
    //Unable it
    oldNumPage.classList.remove('active');    
    oldContentPage.style.display = 'none';
    
    //activar focus numero pagina
    currentNumPage = document.getElementById(pagIndexString+index);    
    currentContentPage = document.getElementById(pageContentString+index);
    currentNumPage.className="active";
    currentContentPage.removeAttribute("style");
}

function previousPage(pagIndexString,pageContentString) {
    oldIndex = getIndex();    
    if(oldIndex[0] !== null) {
        //Old page
        oldNumPage = document.getElementById(pagIndexString+oldIndex[2]);
        oldContentPage = document.getElementById(pageContentString+oldIndex[2]);
        oldNumPage.classList.remove('active');    
        oldContentPage.style.display = 'none';
        
        //New page
        currentNumPage = document.getElementById(pagIndexString+oldIndex[0]);    
        currentContentPage = document.getElementById(pageContentString+oldIndex[0]);
        currentNumPage.className="active";
        currentContentPage.removeAttribute("style");
    }
}

function nextPage(pagIndexString,pageContentString) {
    oldIndex = getIndex();    
    if(oldIndex[1] !== null) {
        //Old page
        oldNumPage = document.getElementById(pagIndexString+oldIndex[2]);
        oldContentPage = document.getElementById(pageContentString+oldIndex[2]);
        oldNumPage.classList.remove('active');    
        oldContentPage.style.display = 'none';
        
        //New page
        currentNumPage = document.getElementById(pagIndexString+oldIndex[1]);    
        currentContentPage = document.getElementById(pageContentString+oldIndex[1]);
        currentNumPage.className="active";
        currentContentPage.removeAttribute("style");
    }    
}

function getIndex() {
    pagination = document.getElementsByClassName('pagination')[0];    
    allButtons = pagination.querySelectorAll('a');    
    previous = null;
    next = null;
    for(var i = 0; i < allButtons.length; i++) {
        if (allButtons[i].className === "active") {
            if ((i-1) > 0) previous = i-1;
            if ((i+1) < allButtons.length-1) next = i+1;
            return [previous,next,i];
        }
    }    
}

/*
 
function changePage(pagIndexString,pageContentString,index) {
    removeActive()[2].classList.remove('active');    
    removeContentActive();
    //activar focus numero pagina
    current = document.getElementById(pagIndexString+index);
    current.className="active";
    //activar el contenido
    current = document.getElementById(pageContentString+index);
    current.className="showPage";
    
}

function previousPage() {
    elements = removeActive();    
    if(elements[0] !== null) {
        elements[2].classList.remove('active');
        elements[0].className="active";
    }
}

function nextPage() {
    elements = removeActive();    
    if (elements[1] !== null) {
        elements[2].classList.remove('active');
        elements[1].className="active";
    }       
}

function removeActive() {
    pagination = document.getElementsByClassName('pagination')[0];
    contentPagination = document.getElementsByClassName('contentPagination')[0];
    allButtons = pagination.querySelectorAll('a');    
    allContent = contentPagination.querySelectorAll('div');
    
    previous = null;
    next = null;
    for(var i = 0; i < allButtons.length; i++) {
        if (allButtons[i].className === "active") {
            if ((i-1) > 0) previous = allButtons[i-1];
            if ((i+1) < allButtons.length-1) next = allButtons[i+1];   
            actual = allButtons[i];
            return [previous,next,actual];
        }
    }
} 
 */

//------------------------------------------------------------------------------

/**
 * Notifica cuantas recetas tienes y el éxito al crear la actual
 * @param {type} tamaño
 * @returns {undefined}
 */
function createForm(tamaño) {
    alert("Creada una receta. Tienes " + (tamaño+1) + " recetas.");
} 

//------------------------------------------------------------------------------

/**
 * Obtiene la imagen seleccionada (input type=file) y la establece en el campo
 * de imagen (para que podamos ver la imagen asignada a la receta)
 * @returns {undefined}
 */
function getImagePath() {
    fileInput = document.getElementById('inputFileImage');
    var fileName = fileInput.value.split(/(\\|\/)/g).pop();
    image = document.getElementById('inputImageItem');
    image.src = ("Images/" + fileName);
}

//------------------------------------------------------------------------------

/**
 * Muestra u oculta el "number textfield" en el filtrado del catalogo de recetas
 * @returns {undefined}
 */
function inputCaloryVisibility() {
    selected = document.getElementById('calorySelect');
    textInput = document.getElementById('caloryValue');
    
    if (selected.checked) textInput.type = "num";
    else textInput.type = "hidden";
        
}

//------------------------------------------------------------------------------



//------------------------------------------------------------------------------

var index = [];
// Array starts with 0 but the id start with 0 so push a dummy value
index.push(0);
// Push 1 at index 1 since one child element is already created
index.push(1);

/**
 * Añade una nueva fila de ingrediente
 * @returns {undefined}
 */
function addkid() {  
  var div = document.createElement('div');
  var id = getID();
  div.setAttribute("id","Div_"+id);  
  div.setAttribute("class","form-control");  
  div.innerHTML = 'Ingrediente ' + id + 
          ': <input type="text" id="ingredient_' + id + '" name="ingredient_' + id + '" required/>' + 
          ' Cantidad: <input type="text" id="ingredient_quantity_' + id + '" name="ingredient_quantity_' + id + '" required/>' + 
          ' Calorías: <input type="number" id="ingredient_calories_' + id + '" name="ingredient_calories_' + id + '" required/>' + 
          ' Tipo: <select id="ingredient_type_' + id + '" name="ingredient_type_' + id + '">' + 
                ' <option value="CARNE">Carne</option> ' +
                ' <option value="PESCADO">Pescado</option> ' + 
                ' <option value="FRUTA">Fruta</option> ' +
                ' <option value="VERDURA">Verdura</option> ' + 
                ' <option value="TUBERCULO">Tuberculo</option> ' + 
                ' <option value="CEREAL">Cereal</option> ' + 
                ' <option value="LEGUMBRE">Legumbre</option> ' + 
                ' <option value="LACTEO">Lacteo</option> ' + 
                ' <option value="OTRO">Otro</option> ' + 
            ' </select> ' +
          ' <input type="button" id="add_kid\(\)_1" onclick="addkid()" value="+" /> ' +
          ' <input type="button" id="rem_kid()_' + id + '" onclick="remkid('+id+')" value="-" />';
  document.getElementById('ingredients_group').appendChild(div);
}

/**
 * Obtiene identificadores únicos y consecutivos (cuando es posible) para nuevas
 * filas de ingredientes
 * @returns {Number}
 */
function getID(){
    var emptyIndex = index.indexOf(-1);
    if (emptyIndex != -1) {
        index[emptyIndex] = emptyIndex;
        return emptyIndex;
    } else {
        emptyIndex = index.length;
        index.push(emptyIndex);
        return emptyIndex;
    }
}

/**
 * Genera tantas filas de ingredientes como indica el parámetro numKids
 * @param {type} numKids
 * @returns {undefined}
 */
function generateKids(numKids) {
    for (i = 0; i < numKids; i++) {
        addkid();
    }
} 

/**
 * Rellena una fila de ingrediente concreta (fieldNumber) con los valores 
 * pasados por parámetro.
 * @param {type} fieldNumber
 * @param {type} ingredient
 * @param {type} quantity
 * @param {type} calories
 * @param {type} type
 * @returns {undefined}
 */
function setIngredientField(fieldNumber, ingredient, quantity, calories, type) {
    document.getElementById('ingredient_'+fieldNumber).value = ingredient;
    document.getElementById('ingredient_quantity_'+fieldNumber).value = quantity;
    document.getElementById('ingredient_calories_'+fieldNumber).value = calories;
    document.getElementById('ingredient_type_'+fieldNumber).value = type;
}

/**
 * Quita la actual fila de ingrediente (sobre la cual se accionó el botón eliminar)
 * @param {type} id
 * @returns {undefined}
 */
function remkid(id) {
    try {
        var element = document.getElementById("Div_"+id)
        element.parentNode.removeChild(element);
        index[id] = -1;
        //id number is = index of the array so we set to -1 to indicate its empty
    }  catch(err){
        alert("id: Div_"+id)
        alert(err)
    }
}