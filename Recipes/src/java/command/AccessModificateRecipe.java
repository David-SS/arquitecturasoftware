
package command;

import ejb.model.singleton.LogSingleton;
import entities.Recipe;
import sessionBeanFacades.RecipeFacade;

public class AccessModificateRecipe extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> CREATE RECIPES ACCESS COMMAND");
        
        recipeFacade = getRecipeFacade();
        getRecipeToModificate();
        forward("/JSP-Pages/ModificateRecipe.jsp");
    }   
    
    private void getRecipeToModificate() {
        Recipe recipeToModificate = recipeFacade.getRecipeById(Integer.parseInt(request.getParameter("id")));
        session.setAttribute("recipeToModificate", recipeToModificate);
    }
}
