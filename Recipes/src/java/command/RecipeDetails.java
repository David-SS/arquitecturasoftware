
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import entities.Recipe;
import sessionBeanFacades.RecipeFacade;


public class RecipeDetails  extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> RECIPE DETAILS COMMAND");
        
        recipeFacade = getRecipeFacade();
        getRecipe();
        forward("/JSP-Pages/RecipeDetails.jsp");
    }

    private void getRecipe() {              
        int recipeId = Integer.parseInt(request.getParameter("id"));
        Recipe recipe = recipeFacade.getRecipeById(recipeId);
        checkRecipeOwner(recipe);
    }
    
    private void checkRecipeOwner(Recipe recipe) {
        int userId = ((AppUser)session.getAttribute("user")).getId();
        if (recipe != null) {
            if (recipe.getUserId().getId() == userId) {
                session.setAttribute("recipeDetails",recipe);
            } else {
                session.setAttribute("catalogRecipeDetails",recipe);
            }
        }
    }
}
