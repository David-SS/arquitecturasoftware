
package sessionBeanFacades;

import entities.AppUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;


@Stateless
public class AppUserFacade extends AbstractFacade<AppUser> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AppUserFacade() {
        super(AppUser.class);
    }
    
    public AppUser getLogedUser(String user) {
        clearCache();
        Query loginQuery = em.createNamedQuery("AppUser.findByLoginParams");
        loginQuery.setParameter("email",user);
        loginQuery.setParameter("username",user);
        List<AppUser> userList = loginQuery.getResultList();
        if (userList.isEmpty())
            return null;
        else 
            return userList.get(0);
    }
    
    public int deleteUserById (int userId) {
        clearCache();
                
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaDelete<AppUser> cDelete = cb.createCriteriaDelete(AppUser.class);
        Root<AppUser> user = cDelete.from(AppUser.class);

        //Aplying where clause with predicate
        cDelete.where(cb.equal(user.get("id"),userId));

        //Doing delete
        return em.createQuery(cDelete).executeUpdate();
    }
    
    public int updateUser (int userId, String name, String surname, String email, String username, String password) {
        clearCache();
        
        CriteriaBuilder cb = em.getCriteriaBuilder();        
        CriteriaUpdate<AppUser> cUpdate = cb.createCriteriaUpdate(AppUser.class);        
        Root<AppUser> user = cUpdate.from(AppUser.class);
        
        //Update fields
        cUpdate.set("name", name);
        cUpdate.set("surname", surname);
        cUpdate.set("email", email);
        cUpdate.set("username", username);
        cUpdate.set("password", password);
        
        //Aplying where clause 
        cUpdate.where(cb.equal(user.get("id"),userId));
        
        //Doing update query
        return em.createQuery(cUpdate).executeUpdate();
    }
}
