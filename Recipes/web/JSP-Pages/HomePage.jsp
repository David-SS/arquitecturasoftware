
<%@page import="entities.AppUser"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP HOME PAGE");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("HomePage.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title> Página Principal </title>
    </head>
    <body>
        
        <%@include file="Includes/Header.jsp" %>
        
        <main>
            <h2 class="ActualPage">Pagina de Inicio</h2>
            <h3 class="titleFormat"> Recetas de cocina: "MakeYourRecipes" </h3>
            <h4 class="titleFormat"> Tus recetas aquí y en cualquier lado. Almacena, comparte, cocina.</h4>            
            
            <div class="contentMargin">                
                <p>
                    Bienvenido a nuestra página de gestión de recetas de cocina. Si
                    eres una persona a la que le gusta mucho la cocina puede que este
                    sea el sitio que buscabas. No nos quedamos ahí, te garantizamos
                    que si te gusta la cocina y eres una persona a la que le gusta
                    apuntar todas sus recetas y compartirlas, este es el sitio que 
                    estabas buscando.
                <p/>

                <p>
                    En nuestra aplicacion puedes hacer infinidad de cosas:
                <p/>

                <ul>
                    <li>
                        Crear recetas propias: Esto lo haces añadiendo imágenes, 
                        descripción, datos estadisticos como tiempo de preparación 
                        y dificultad, etc.
                    </li>
                    <li>
                        Gestión de visibilidad de recetas: Si no te gusta compartir
                        recetas y solo las apuntas para no olvidarte, te ofertamos esa
                        posibilidad. No todas tus recetas tienen por qué estar al 
                        alcance de todos, puedes tenerlas bajo llave. En este caso, 
                        nosotros somos tu llave.
                    </li>
                    <li>
                        Ver recetas y guardarlas como favoritas: Puedes llamarlo como sea, 
                        pero la utilidad que te ofertamos va más allá. Comprendemos
                        las recetas como una forma de red social en la que en lugar
                        de compartir "Selfies" compartes tus recetas.
                    </li>
                    <li>
                        Lo más importante, es totalmente gratís.. la ofertamos al
                        público de buena voluntad..
                    </li>
                </ul>

                <p>
                    ¿Todavía te lo estas pensando? Create una cuenta de colaborador para
                    que puedas no solo ver recetas, sino crear, compartir y valorarlas.
                <p/>
                
            </div>
            
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>    
</html>


