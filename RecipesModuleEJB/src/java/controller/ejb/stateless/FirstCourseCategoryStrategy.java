
package controller.ejb.stateless;

import ejb.model.singleton.LogSingleton;
import entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import strategy.CategoryStrategy;

@Stateless
@LocalBean
public class FirstCourseCategoryStrategy extends CategoryStrategy {

    private LogSingleton ls;
    public FirstCourseCategoryStrategy() {
        try {
            this.ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(FirstCourseCategoryStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
    @Override
    public List<Recipe> getCategoryRecipes(List<Recipe> recipeCatalog) {
        ls.addLogMessage("FirstCourseCategoryStrategy:getCategoryRecipes");
        List<Recipe> filteredRecipes = new ArrayList<>();
        for (Recipe recipe : recipeCatalog) {
            if (recipe.getType().equals("Primer Plato")) filteredRecipes.add(recipe);
        }
        return filteredRecipes;
    }
        
    @PostConstruct
    void postConstruct() {
        ls.addLogMessage("FirstCourseCategoryStrategy:POSTCONTRUCT");
    }    
    @PreDestroy
    void preDestroy() {
        ls.addLogMessage("FirstCourseCategoryStrategy:PREDESTROY");
    } 
}
