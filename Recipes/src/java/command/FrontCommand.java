
package command;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionBeanFacades.AppUserFacade;
import sessionBeanFacades.RecipeFacade;
import sessionBeanFacades.IngredientFacade;
import sessionBeanFacades.StepFacade;
import ejb.model.singleton.LogSingleton;
import ejb.model.singleton.StatisticsSingleton;
import ejb.model.singleton.RecipeCatalog;
import entities.AppUser;
import java.io.UnsupportedEncodingException;
import sessionBeanFacades.FavoriteRecipeFacade;
import sessionBeanFacades.FavoriteUserFacade;
import sessionBeanFacades.RecipeLikeFacade;


public abstract class FrontCommand {
    
    protected ServletContext context;
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;
    
    public void init(ServletContext context, HttpServletRequest request,HttpServletResponse response){
        this.context = context;
        this.request = request;
        this.response = response;
        this.session = request.getSession();
        try {
            this.request.setCharacterEncoding( "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    abstract public void process();
    
    public void forward(String target) {
        try {
            RequestDispatcher dp = context.getRequestDispatcher(target);
            dp.forward(request,response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////// Facade Getters //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////   
    
    protected AppUserFacade getUserFacade() {
        AppUserFacade appUserFacade = null;
        try {
            appUserFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/AppUserFacade!sessionBeanFacades.AppUserFacade");
        } catch (NamingException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appUserFacade;
    }
    
    protected RecipeFacade getRecipeFacade() {
        RecipeFacade rFacade = null;
        try {
            rFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/RecipeFacade!sessionBeanFacades.RecipeFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rFacade;
    }
    
    protected IngredientFacade getIngredientFacade() {
        IngredientFacade iFacade = null;
        try {
            iFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/IngredientFacade!sessionBeanFacades.IngredientFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return iFacade;
    }
    
    protected StepFacade getStepsFacade() {
        StepFacade sFacade = null;
        try {
            sFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StepFacade!sessionBeanFacades.StepFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sFacade;
    }
    
    protected FavoriteRecipeFacade getFavoriteRecipeFacade() {
        FavoriteRecipeFacade fFacade = null;
        try {
            fFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/FavoriteRecipeFacade!sessionBeanFacades.FavoriteRecipeFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fFacade;
    }
    
    protected FavoriteUserFacade getFavoriteUserFacade() {
        FavoriteUserFacade fuFacade = null;
        try {
            fuFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/FavoriteUserFacade!sessionBeanFacades.FavoriteUserFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fuFacade;
    }
    
    protected RecipeLikeFacade getRecipeLikeFacade() {
        RecipeLikeFacade rlFacade = null;
        try {
            rlFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/RecipeLikeFacade!sessionBeanFacades.RecipeLikeFacade");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rlFacade;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    /////////////////////// Other Beans Getters ////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////   
    
    protected LogSingleton getLogBean() {
        LogSingleton logSingleton = null;
        try {
            logSingleton = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logSingleton;
    }
    
    protected StatisticsSingleton getStatisticsBean() {
        StatisticsSingleton statisticsSingleton = null;
        try {
            statisticsSingleton = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statisticsSingleton;
    }
    
    protected RecipeCatalog getRecipeCatalogBean() {
        RecipeCatalog recipeCatalog = null;
        try {
            recipeCatalog = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/RecipeCatalog!ejb.model.singleton.RecipeCatalog");
        } catch (NamingException ex) {
            Logger.getLogger(FrontCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return recipeCatalog;
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////// Other Functions //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////   
    
    protected String getSessionUsername() {
        AppUser actualUser = (AppUser)session.getAttribute("user");
        return actualUser.getUsername();
    }
   
}
