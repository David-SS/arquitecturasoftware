package ejb.model.singleton;

import controller.ejb.stateless.AllCatalogStrategy;
import controller.ejb.stateless.AllCategoryStrategy;
import entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import strategy.CategoryStrategy;
import strategy.FilterAndOrderStrategy;

@Singleton
@LocalBean
public class RecipeCatalog {
    FilterAndOrderStrategy faos;
    CategoryStrategy cs;
    List<Recipe> recipeCatalog;
    
    public RecipeCatalog() {
        recipeCatalog = new ArrayList<>();
        this.faos = new AllCatalogStrategy();
        this.cs = new AllCategoryStrategy();
    }
        
    @Lock(LockType.WRITE)
    public void setFilterAndOrderStrategy(FilterAndOrderStrategy faos) {
        this.faos = faos;
    }
    
    @Lock(LockType.WRITE)
    public void setCategoryStrategy(CategoryStrategy cs) {
        this.cs = cs;
    }
    
    @Lock(LockType.WRITE)
    public void setCatalogList(List<Recipe> recipeCatalog) {
        this.recipeCatalog = new ArrayList<>();
        if (recipeCatalog != null) this.recipeCatalog = recipeCatalog;
    }
    
    @Lock(LockType.READ)
    public List<Recipe> applyFilter() {
        return cs.getCategoryRecipes(faos.getFilteredResult(recipeCatalog));        
    }
}