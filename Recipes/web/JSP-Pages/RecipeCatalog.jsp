
<%@page import="entities.Recipe"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="ejb.model.singleton.RecipeCatalog"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP CATALOG");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("RecipeCatalog.jsp");
%>


<!DOCTYPE html>
<%! final int elemsPerPage = 4; %>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Catálogo de Recetas</title>
    </head>
    <body>      
        
        <%@include file="Includes/Header.jsp" %>
        
        <% 
            RecipeCatalog recipeCatalog = (RecipeCatalog) InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/RecipeCatalog!ejb.model.singleton.RecipeCatalog"); 
            List<Recipe> recipeList = recipeCatalog.applyFilter();  
        %> 
        
        <main>
            <h2 class="ActualPage">Catálogo de Recetas</h2>
            
            <% if (recipeList.size() == 0) { %>
            <div class="contentMargin">
                <p> 
                    No existen recetas creadas que coincidan los valores de búsqueda.
                </p>               
            </div>          
            
            <% } else { %>     
            
            <!-- ENCABEZADO: NAVEGACION RECETAS Y FORMULARIO DE BUSQUEDA-->
            <div class="contentMargin thumbnail">
                <div class="row col-sm-12 ">
                    <form action="FrontController">
                        <div class="row col-sm-12">
                            <div class="inside_elements_margin">
                                <input placeholder="Nombre de receta" class="form-control" type="text" name="filter">
                            </div>  
                            <div class="inside_elements_margin">
                                <input type="hidden" name="command" value="RecipeCatalog">
                                <input type="submit" value="Filtrar" class="btn btn-secondary">
                            </div>
                            <div class="pagination">
                                <a href="#noDest" onclick="previousPage('PageNum','PageContent')">&laquo;</a>
                                <%
                                    int index;
                                    String indexId="",onChangeId=""; 
                                    for (index = 0; index < recipeList.size()/elemsPerPage; index++) {
                                        indexId= "\"PageNum"+(index+1)+"\"";
                                        onChangeId = "\"changePage("+"'PageNum','PageContent','"+ (index+1) + "')\"";
                                        if (index != 0) out.println("<a href=\"#noDest\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                        else out.println("<a href=\"#noDest\" id=" + indexId + " class=\"active\" onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                    }
                                    if (recipeList.size()%elemsPerPage != 0) {
                                        indexId= "\"PageNum"+(index+1)+"\"";
                                        onChangeId = "\"changePage("+"'PageNum','PageContent','"+ (index+1) + "')\"";
                                        if (index == 0) out.println("<a href=\"#noDest\" class=\"active\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                        else out.println("<a href=\"#noDest\" id=" + indexId + " onclick=" + onChangeId + ">" + (index+1) + "</a>");
                                    }
                                %>
                                <a href="#noDest" onclick="nextPage('PageNum','PageContent')">&raquo;</a>
                            </div>
                        </div>
                        <div class="inside_elements_margin"><input type="checkbox" name="criteria" value="criteria"> Criteria</div>    
                        <div class="row" onchange="inputCaloryVisibility();" class="radio-group">
                            <div class="form-check">
                                <input checked=checked name="catalogFilter" value="AllCatalog" type="radio" class="with-gap" required>
                                <label>Sin Filtro</label>
                            </div>
                            <div class="form-check">
                                <input id="calorySelect" name="catalogFilter" value="OnlyCalory" type="radio" class="with-gap" required>
                                <input id="caloryValue" name="caloryField" type="hidden">
                                <label>Calorias máximas</label>
                            </div>
                            <div class="form-check">
                                <input name="catalogFilter" value="OnlyVegetable" type="radio" class="with-gap">
                                <label>Recetas para vegetarianos</label>
                            </div>      
                            <div class="form-check">
                                <input name="catalogFilter" value="OnlyAntidairy" type="radio" class="with-gap">
                                <label>Recetas sin lactosa</label>
                            </div>    
                        </div> 
                        <div class="row" class="radio-group">
                            <div class="form-check">
                                <input checked=checked name="catalogCategoryFilter" value="AllCategories" type="radio" class="with-gap" required>
                                <label>Todas las categorías</label>
                            </div>
                            <div class="form-check">
                                <input name="catalogCategoryFilter" value="Appetizer" type="radio" class="with-gap" required>
                                <label>Aperitivo</label>
                            </div>
                            <div class="form-check">
                                <input name="catalogCategoryFilter" value="FirstCourse" type="radio" class="with-gap">
                                <label>Primer Plato</label>
                            </div>      
                            <div class="form-check">
                                <input name="catalogCategoryFilter" value="SecondCourse" type="radio" class="with-gap">
                                <label>Segundo Plato</label>
                            </div>   
                            <div class="form-check">
                                <input name="catalogCategoryFilter" value="Dessert" type="radio" class="with-gap">
                                <label>Postre</label>
                            </div>  
                        </div>
                    </form>
                </div>
                                
                
                <!-- LISTADO DE RECETAS SEGMENTADAS POR PAGINAS -->
                <% 
                    int count = 0;
                    int elemsPerRow = elemsPerPage;
                    for (Recipe recipe : recipeList) {
                        if (count == 0) 
                            out.println("<div class=\"row\" id=\"PageContent"+((count/elemsPerRow)+1)+"\">");
                        if (count%elemsPerPage == 0 && count != 0)
                            out.println("<div style=\"display: none;\" class=\"row\" id=\"PageContent"+((count/elemsPerRow)+1)+"\">");
                %>
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <div class="card">
                            <a href="FrontController?command=RecipeDetails&id=<%= recipe.getId() %>">
                                <img class="card-img-top" src=" <%= recipe.getImageUri()%> ">
                            </a>
                            <div class="card-block">
                                <h4 class="card-title"><%= recipe.getName()%></h4>
                                <div class="meta float-left">
                                    <p><%= recipe.getLastMod()%></p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <span class="float-right">Dificultad: <%= recipe.getDifficulty()%></span>
                                <span><i class=""></i>Tiempo: <%= recipe.getMinutes()%></span>
                            </div>
                        </div>
                    </div>                        
                <%
                        count++;
                        elemsPerRow--;
                        if (elemsPerRow == 0) {
                            elemsPerRow = elemsPerPage;
                            out.println("</div>");
                        }
                    }
                    out.println("</div>");
                %>
            </div>  
                
            <% } %>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>
</html>

