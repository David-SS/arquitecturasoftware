
<%@page import="sessionBeanFacades.FavoriteRecipeFacade"%>
<%@page import="sessionBeanFacades.IngredientFacade"%>
<%@page import="sessionBeanFacades.StepFacade"%>
<%@page import="entities.Recipe"%>
<%@page import="entities.Step"%>
<%@page import="entities.Ingredient"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP RECIPE DETAILS");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("RecipeDetails.jsp");
%>

<!DOCTYPE html>
<%! Recipe recipeDetails; %>
<%! Recipe catalogRecipeDetails; %>
<html>    
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Detalles de Receta</title>
    </head>
    <body>

        <%@include file="Includes/Header.jsp" %>

        <main>
            <%
                FavoriteRecipeFacade favoriteRecipeFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/FavoriteRecipeFacade!sessionBeanFacades.FavoriteRecipeFacade");
                StepFacade stepFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StepFacade!sessionBeanFacades.StepFacade");
                IngredientFacade ingredientFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/IngredientFacade!sessionBeanFacades.IngredientFacade");
                recipeDetails = (Recipe) session.getAttribute("recipeDetails");
                session.removeAttribute("recipeDetails");
                catalogRecipeDetails = (Recipe) session.getAttribute("catalogRecipeDetails");
                session.removeAttribute("catalogRecipeDetails");
            %>

            <% if (recipeDetails != null || catalogRecipeDetails != null) { %>

            <h2 class="ActualPage">Detalles de Receta</h2>    

            <div class="row contentMargin">

                <% 
                    if (catalogRecipeDetails != null) { 
                        recipeDetails = catalogRecipeDetails;
                    } 
                %>   

                <div class="inside_elements_margin">
                    <figure class="figure">
                        <img id="inputImageItem" src="<%= recipeDetails.getImageUri()%>" width="600" height="auto">
                    </figure>
                </div>

                <div class="inside_elements_margin limit_width"> 
                    <h1 class="titleFormat"> <%= recipeDetails.getName() %></h1>

                    <br>

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active show" href="#Detalles" role="tab" data-toggle="tab">Detalles</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Ingredientes" role="tab" data-toggle="tab">Ingredientes</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Pasos" role="tab" data-toggle="tab">Pasos</a>
                        </li>
                        <% if (catalogRecipeDetails == null) { %>
                            <li class="nav-item">
                                <a class="btn btn-secondary" href="FrontController?command=AccessModificateRecipe&id=<%=recipeDetails.getId()%>">Modificar</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-secondary" href="FrontController?command=RemoveRecipe&id=<%=recipeDetails.getId()%>">Borrar</a>
                            </li>
                        <% } %>
                        <li>
                            <% if (favoriteRecipeFacade.getFavoriteRecipeById(recipeDetails, sessionUser) != null) { %>
                                <a class="btn btn-secondary" href="FrontController?command=FavoriteRecipeDelete&id=<%=recipeDetails.getId()%>">Eliminar Favorita</a>
                            <% } else { %>
                                <a class="btn btn-secondary" href="FrontController?command=FavoriteRecipeAdd&id=<%=recipeDetails.getId()%>">Añadir Favorita</a>
                            <% } %>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active show" id="Detalles"> 
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Descripción</div>
                                <div class="panel-body"> <%= recipeDetails.getDescription()%> </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Tiempo de preparación</div>
                                <div class="panel-body"> <%= recipeDetails.getMinutes()%> Minutos </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Dificultad</div>
                                <div class="panel-body"> <%= recipeDetails.getDifficulty()%> </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Tipo</div>
                                <div class="panel-body"> <%= recipeDetails.getType()%> </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Privado</div>
                                <div class="panel-body"> 
                                    <% 
                                        if (recipeDetails.getPrivate1() == 0)  
                                            out.println("Publica");
                                        else 
                                            out.println("Privada");
                                    %> 
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="Ingredientes"> 
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Ingredientes</div>
                                <div class="panel-body"> 
                                    <% 
                                        for (Ingredient ingredient : ingredientFacade.getRecipeIngredientsById(recipeDetails)) {
                                            out.println(ingredient.getIngredientNum() + ") " + ingredient.getName() + " (" + ingredient.getQuantity()+ ")<br>");
                                        }           
                                    %>
                                </div>
                            </div>                      
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="Pasos"> 
                            <div class="panel panel-default">
                                <div class="sub-topic panel-heading">Pasos</div>
                                <div class="panel-body"> 
                                    <% 
                                        for (Step step : stepFacade.getRecipeStepsById(recipeDetails)) {
                                            out.println(step.getStepNum() + ") " + step.getStepDesc() + "<br>");
                                        }           
                                    %>
                                </div>
                            </div>                      
                        </div>
                    </div>

                    <% } else { %>
                        <h1> Detalles de la Receta </h1>
                        <p>
                            Lo sentimos, la receta a la que intenta acceder no está disponible.
                            Inténtelo de nuevo más tarde.
                        </p>
                    <% } %>
                </div>
            </div>  
        </main>

        <%@include file="Includes/Footer.jsp" %>

    </body>
</html>
