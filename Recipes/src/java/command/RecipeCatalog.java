
package command;

import controller.ejb.stateless.AllCatalogStrategy;
import controller.ejb.stateless.AllCategoryStrategy;
import controller.ejb.stateless.AppetizerCategoryStrategy;
import controller.ejb.stateless.DessertCategoryStrategy;
import controller.ejb.stateless.FirstCourseCategoryStrategy;
import controller.ejb.stateless.OnlyAntidairyStrategy;
import controller.ejb.stateless.OnlyCaloryStrategy;
import controller.ejb.stateless.OnlyVegetableStrategy;
import controller.ejb.stateless.SecondCourseCategoryStrategy;
import ejb.model.singleton.LogSingleton;
import entities.Recipe;
import java.util.List;
import sessionBeanFacades.RecipeFacade;

public class RecipeCatalog extends FrontCommand{  
    
    private ejb.model.singleton.RecipeCatalog rc;
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    
    @Override
    public void process() {
        rc = getRecipeCatalogBean();
        ls = getLogBean();
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> RECIPE CATALOG COMMAND");
        
        recipeFacade = getRecipeFacade();
        
        applyFilterOptions();
        applyCategoryOptions();
        setCurrentCatalogList();
        forward("/JSP-Pages/RecipeCatalog.jsp");
    }

    private void applyFilterOptions() {     
        String filter = request.getParameter("catalogFilter");
        if (filter != null) {
            switch (filter) {
                case "OnlyCalory":
                    String calories = request.getParameter("caloryField");
                    if (calories.equals(""))
                        rc.setFilterAndOrderStrategy(new OnlyCaloryStrategy());
                    else 
                        rc.setFilterAndOrderStrategy(new OnlyCaloryStrategy(Integer.parseInt(calories)));
                    break;
                case "OnlyVegetable":
                    rc.setFilterAndOrderStrategy(new OnlyVegetableStrategy());
                    break;
                case "OnlyAntidairy":
                    rc.setFilterAndOrderStrategy(new OnlyAntidairyStrategy());
                    break;
                default:
                    rc.setFilterAndOrderStrategy(new AllCatalogStrategy());
            }
        } else {
            rc.setFilterAndOrderStrategy(new AllCatalogStrategy());
        }
    }

    private void applyCategoryOptions() {
        String filter = request.getParameter("catalogCategoryFilter");
        if (filter != null) {
            switch (filter) {
                case "Appetizer":
                    rc.setCategoryStrategy(new AppetizerCategoryStrategy());
                    break;
                case "FirstCourse":
                    rc.setCategoryStrategy(new FirstCourseCategoryStrategy());
                    break;
                case "SecondCourse":
                    rc.setCategoryStrategy(new SecondCourseCategoryStrategy());
                    break;
                case "Dessert":
                    rc.setCategoryStrategy(new DessertCategoryStrategy());
                    break;
                default:
                    rc.setCategoryStrategy(new AllCategoryStrategy());
            }
        } else {
            rc.setCategoryStrategy(new AllCategoryStrategy());
        }
    }

    private void setCurrentCatalogList() {
        String filter = request.getParameter("filter");
        String criteria = request.getParameter("criteria");
        List<Recipe> catalogRecipes;
        if (filter != null) {
            if (criteria != null && criteria.equals("criteria")) {
                catalogRecipes = recipeFacade.getRecipeCatalogFilteredCriteria(filter);
                System.out.println("criteria");
            } else {
                catalogRecipes = recipeFacade.getRecipeCatalogFiltered(filter);
                System.out.println("normal");   
            }
        } 
        else {
            catalogRecipes = recipeFacade.getRecipeCatalogFiltered("");
            System.out.println("default");   
        }        
        rc.setCatalogList(catalogRecipes);
       
    }
}
