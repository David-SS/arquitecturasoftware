
<%@page import="entities.Step"%>
<%@page import="entities.Ingredient"%>
<%@page import="sessionBeanFacades.StepFacade"%>
<%@page import="sessionBeanFacades.IngredientFacade"%>
<%@page import="entities.Recipe"%>
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP MODIFICATE RECIPE");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("ModificateRecipe.jsp");    
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Modificar Receta</title>
    </head>
    <body>
        
        <%@include file="Includes/Header.jsp" %>
        
        <% 
            Recipe actualRecipe = (Recipe)session.getAttribute("recipeToModificate"); 
            IngredientFacade ingredientFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/IngredientFacade!sessionBeanFacades.IngredientFacade");
            StepFacade stepFacade = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StepFacade!sessionBeanFacades.StepFacade");
            List<Ingredient> ingredients = ingredientFacade.getRecipeIngredientsById(actualRecipe);
            List<Step> steps = stepFacade.getRecipeStepsById(actualRecipe);
        %>
        
        <main>
            <h2 class="ActualPage">
                <a id="backButton" href="FrontController?command=RecipeDetails&id=<%= actualRecipe.getId() %>">
                    <img src="Images/ForWebPage/back.png" width="40" height="auto">
                </a>
                Modificar Receta
            </h2>
            <div class="contentMargin">
                <form  action="FrontController">
                     
                <div class="row">
                    
                    <div class="inside_elements_margin">
                        <img id="inputImageItem" src="<%= actualRecipe.getImageUri()%>" width="600" height="auto">
                        <div class="form-group">
                            <input id="inputFileImage" name="inputFileImage" type="file" onchange="getImagePath();">
                        </div>
                    </div>
                    
                    <div class="inside_elements_margin">
                        <div class="form-group">
                            <label for="pwd"> Nombre: </label> 
                            <input class="form-control" type="text" name="name" required value="<%= actualRecipe.getName() %>">
                        </div>

                        <div class="form-group">
                            <label for="pwd"> Descripción: </label> 
                            <textarea class="form-control" rows="4" cols="50" name="description" 
                                placeholder="Receta muy sana a base de.."><%= actualRecipe.getDescription()%></textarea>
                        </div>                  

                        <div id="ingredients_group" class="form-group">
                            <label for="pwd"> Ingredientes: </label>
                            <div class="form-control">
                                Ingrediente 1:
                                <input value="IngredienteEjemplo" type="text" id="ingredient_1" name="ingredient_1" required>
                                Cantidad: <input value="12" type="text" id="ingredient_quantity_1" name="ingredient_quantity_1" required/>
                                Calorías: <input value="12" type="number" id="ingredient_calories_1" name="ingredient_calories_1" required/>
                                Tipo: <select id="ingredient_type_1" name="ingredient_type_1">
                                    <option value="CARNE">Carne</option> 
                                    <option value="PESCADO">Pescado</option> 
                                    <option value="FRUTA">Fruta</option>
                                    <option value="VERDURA">Verdura</option> 
                                    <option value="LACTEO">Lacteo</option> 
                                    <option value="CEREAL">Cereal</option> 
                                    <option value="LEGUMBRE">Legumbre</option> 
                                    <option value="OTRO">Otro</option> 
                                </select>
                                <input type="button" id="add_kid\(\)_1" onclick="addkid()" value="+" />
                            </div> 
                        </div>                    
                        <script>generateKids( <%= ingredients.size() - 1 %> );</script>                    
                        <% 
                            int count = 1;
                            for (Ingredient ingredient : ingredients) { 
                        %>
                        <script>
                            setIngredientField(
                            <%=
                                "'" + count + "','" + ingredient.getName() + "','" + 
                                ingredient.getQuantity()+ "','" +
                                ingredient.getCalories()+ "','" + 
                                ingredient.getType()+"'"
                            %>
                            );
                        </script>
                        <%
                            count++;
                            }
                        %>

                        <div class="form-group">
                            <label for="pwd"> Pasos: </label>
                            <textarea class="form-control" rows="4" cols="50" name="steps"
                                placeholder="Paso1, Paso2, Paso3, etc" required><%
                                String stepString = "";
                                for (Step step : steps) {
                                    String stepDesc = step.getStepDesc();
                                    stepString += stepDesc.substring(0, stepDesc.length()-1) + "," + stepDesc.substring(stepDesc.length()-1, stepDesc.length());
                                }       
                                out.println(stepString.substring(0,stepString.length()-2));
                            %></textarea>
                        </div>
                            
                        <div class="form-group">
                            <label for="pwd">Tiempo de preparación (minutos):</label> 
                            <input class="form-control" type="number" name="minutes" required value="<%= actualRecipe.getMinutes()%>">
                        </div>

                        <div class="row">
                            <div class="radio-group inside_elements_margin">
                                <label> Dificultad: </label>
                                <div class="form-check">
                                    <input name="difficulty" value="Facil" type="radio" required class="with-gap" 
                                           <% 
                                               if ((actualRecipe.getDifficulty()).equals("Facil")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Fácil</label>
                                </div>
                                <div class="form-check">
                                    <input name="difficulty" value="Media" type="radio" class="with-gap"
                                           <% 
                                               if ((actualRecipe.getDifficulty()).equals("Media")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Media</label>
                                </div>
                                <div class="form-check">
                                    <input name="difficulty" value="Dificil" type="radio" class="with-gap"
                                           <% 
                                               if ((actualRecipe.getDifficulty()).equals("Dificil")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Difícil</label>
                                </div>                        
                            </div> 

                            <div class="radio-group inside_elements_margin">
                                <label> Tipo de plato: </label>
                                <div class="form-check">
                                    <input name="recipeType" value="Aperitivo" type="radio" class="with-gap" required
                                           <% 
                                               if ((actualRecipe.getType()).equals("Aperitivo")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Aperitivo</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Primer Plato" type="radio" required class="with-gap"
                                           <% 
                                               if ((actualRecipe.getType()).equals("Primer Plato")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Primer plato</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Segundo Plato" type="radio" class="with-gap"
                                           <% 
                                               if ((actualRecipe.getType()).equals("Segundo Plato")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Segundo plato</label>
                                </div>
                                <div class="form-check">
                                    <input name="recipeType" value="Postre" type="radio" class="with-gap"
                                           <% 
                                               if ((actualRecipe.getType()).equals("Postre")) 
                                                    out.println(" checked=checked");
                                           %>>
                                    <label>Postre</label>
                                </div>                        
                            </div>          
                        </div>

                        <div class="form-group">
                            <input type="checkbox" name="private" value="true" <%
                                if (actualRecipe.getPrivate1() == 1) 
                                    out.println(" checked=checked");
                            %>> 
                            <label for="pwd"> Privado </label> 
                        </div>

                        <input type="hidden" name="id" value="<%= actualRecipe.getId() %>">
                        <input type="hidden" name="command" value="ModificateRecipe">
                        <input type="submit" class="btn btn-primary" value="Guardar Cambios">
                    </div>
                </div>
                </form>
            </div>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
                     
    </body>
</html>