
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    AppUser sessionUser = (AppUser)session.getAttribute("user");
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton"); 
    ls.addLogMessage("USUARIO: " + sessionUser.getUsername() + " --> JSP LOG");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessage("USER_VISITED_PAGES - " + sessionUser.getUsername());
    statistics.addLogMessageForPage("Log.jsp");
%>   

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Log</title>
    </head>
    <body>     
        
        <%@include file="Includes/Header.jsp" %>
          
        <main>
            <h2 class="ActualPage">Log del sistema</h2>
            <div class="contentMargin">
                <% for (String log : ls.getLogMessages()) { %>
                    <p> <%= log %> </p>   
                <% } %>
            </div>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>
</html>

