
package controller.ejb.stateless;

import ejb.model.singleton.LogSingleton;
import entities.Ingredient;
import entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import strategy.FilterAndOrderStrategy;

@Stateless
@LocalBean
public class OnlyVegetableStrategy extends FilterAndOrderStrategy {

    private LogSingleton ls;
    public OnlyVegetableStrategy() {
        try {
            this.ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
        } catch (NamingException ex) {
            Logger.getLogger(OnlyVegetableStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Recipe> getFilteredResult(List<Recipe> recipeCatalog) {
        ls.addLogMessage("OnlyVegetableStrategy:getFilteredResult");
        List<Recipe> filteredRecipes = new ArrayList<>();
        for (Recipe recipe : recipeCatalog) {
            if (notHasMeatFish(recipe)) filteredRecipes.add(recipe);
        }
        return filteredRecipes;
    }

    private boolean notHasMeatFish(Recipe recipe) {
        for (Ingredient ingredient : recipe.getIngredientCollection()) {
            ls.addLogMessage("OnlyVegetableStrategy:notHasMeatFish");
            if ("CARNE".equals(ingredient.getType()) || "PESCADO".equals(ingredient.getType()))
                return false;
        }
        return true;
    }
        
    @PostConstruct
    void postConstruct() {
        ls.addLogMessage("OnlyVegetableStrategy:POSTCONTRUCT");
    }    
    @PreDestroy
    void preDestroy() {
        ls.addLogMessage("OnlyVegetableStrategy:PREDESTROY");
    } 

}
