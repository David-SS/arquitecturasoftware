
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import entities.Ingredient;
import entities.Recipe;
import entities.Step;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import sessionBeanFacades.IngredientFacade;
import sessionBeanFacades.RecipeFacade;
import sessionBeanFacades.StepFacade;

public class CreateRecipe extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    private IngredientFacade ingredientFacade;
    private StepFacade stepFacade;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> CREATE RECIPE COMMAND");
        
        recipeFacade = getRecipeFacade();
        ingredientFacade = getIngredientFacade();
        stepFacade = getStepsFacade();
        recipeHelper();
        forward("/JSP-Pages/RecipeList.jsp");
    }

    private void recipeHelper() {
        Recipe newRecipe = persistRecipe();
        session.setAttribute("recipeDetails",newRecipe);
    }

    private Recipe persistRecipe() {                
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        int minutes = Integer.parseInt(request.getParameter("minutes"));
        String difficulty = request.getParameter("difficulty");
        String type = request.getParameter("recipeType");       
        String imagePath = request.getParameter("inputFileImage"); 
        if (imagePath.equals("")) imagePath = "Images/default.png";
        else imagePath = "Images/" + imagePath;
        int privateRecipe = 0;
        if (request.getParameter("private") != null) privateRecipe = 1;
        
        //With simple create
        //Recipe newRecipe = new Recipe (name, description, minutes, difficulty, privateRecipe, new Date(), type, imagePath);
        //newRecipe.setUserId((AppUser)session.getAttribute("user"));
        //recipeFacade.create(newRecipe);
        
        //With native sql
        AppUser user = (AppUser) session.getAttribute("user");
        Recipe newRecipe = recipeFacade.createRecipe(name, description, minutes, difficulty, privateRecipe, new Date(), type, imagePath,user);
            
        createIngredientAndStep(newRecipe);
        
        return newRecipe;
    }
    
    private void createIngredientAndStep(Recipe newRecipe) {
        List<Ingredient> ingredients = getIngredientsList(newRecipe);
        for (Ingredient ingredient : ingredients)
            ingredientFacade.create(ingredient);
        
        List<Step> steps = getStepList(newRecipe);
        for (Step step : steps)
            stepFacade.create(step);
    }
    
    private List<Ingredient> getIngredientsList(Recipe recipe) {
        List<Ingredient> ingredientList = new ArrayList<>();
        int ingredientCount = 1;
        boolean moreIngredients = true;
        while (moreIngredients) {
            if (request.getParameter("ingredient_"+Integer.toString(ingredientCount)) == null) {
                moreIngredients = false;
            } else {
                String name = request.getParameter("ingredient_"+Integer.toString(ingredientCount));
                String quantity = request.getParameter("ingredient_quantity_"+Integer.toString(ingredientCount));
                String calories = request.getParameter("ingredient_calories_"+Integer.toString(ingredientCount));
                String type = request.getParameter("ingredient_type_"+Integer.toString(ingredientCount));
                Ingredient newIngredient = new Ingredient(name,quantity,calories,type,ingredientCount);   
                newIngredient.setRecipeId(recipe);
                ingredientList.add(newIngredient);
            }
            ingredientCount++;
        }
        return ingredientList;
    }    
    
    private List<Step> getStepList(Recipe recipe) {
        List<Step> stepList = new ArrayList<>();
        int stepCount = 1;
        for (String desc : request.getParameter("steps").split("[,.;:/|]\\s+")) {
            desc =  desc + "\n";
            Step newStep = new Step(desc, stepCount);
            newStep.setRecipeId(recipe);
            stepList.add(newStep);
            stepCount++;
        }
        return stepList;
    }
}
