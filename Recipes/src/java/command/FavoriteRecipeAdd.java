
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import entities.FavoriteRecipe;
import entities.Recipe;
import sessionBeanFacades.FavoriteRecipeFacade;
import sessionBeanFacades.RecipeFacade;

public class FavoriteRecipeAdd extends FrontCommand{
    
    private LogSingleton ls;
    private RecipeFacade recipeFacade;
    private FavoriteRecipeFacade favoriteRecipeFacade;
    
    @Override
    public void process() {
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> FAVORITE RECIPE ADD COMMAND");
        
        recipeFacade = getRecipeFacade();
        favoriteRecipeFacade = getFavoriteRecipeFacade();        
        addToFavorites();
        forward("/JSP-Pages/FavoriteRecipeList.jsp");
    }
    
    private void addToFavorites() {
        Recipe recipeToAdd = recipeFacade.getRecipeById(Integer.parseInt(request.getParameter("id")));
        FavoriteRecipe favoriteRecipe = new FavoriteRecipe();
        favoriteRecipe.setRecipeId(recipeToAdd);
        favoriteRecipe.setUserId((AppUser)session.getAttribute("user"));
        favoriteRecipeFacade.create(favoriteRecipe);
    }
}
