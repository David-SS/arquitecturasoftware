
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("--> **JSP UNKNOWN COMMAND**");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessageForPage("UnknownCommand.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title>Error</title>
    </head>
    <body>        
        
        <%@include file="Includes/Header.jsp" %>
                
        <main>
            <h1  class="titleFormat"> Lo sentimos, ocurrió un error </h1>
            <div class="contentMargin">
                <p> 
                    Se ha intentado realizar una acción no disponible. Por favor, 
                    revise su acción o inténtelo de nuevo más tarde.
                </p>
                <a href="FrontController?command=HomePage" class="btn btn-light"> Volver a Inicio </a>
            </div>
        </main>
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>
</html>
