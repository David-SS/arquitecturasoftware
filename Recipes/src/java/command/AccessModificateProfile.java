
package command;

import ejb.model.singleton.LogSingleton;

public class AccessModificateProfile extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> MODIFICATE PROFILE ACCESS COMMAND");      
        forward("/JSP-Pages/ModificateProfile.jsp");
    }   
}
