
package sessionBeanFacades;

import entities.AppUser;
import entities.FavoriteUser;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class FavoriteUserFacade extends AbstractFacade<FavoriteUser> {

    @PersistenceContext(unitName = "RecipesModuleEJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FavoriteUserFacade() {
        super(FavoriteUser.class);
    }
}
