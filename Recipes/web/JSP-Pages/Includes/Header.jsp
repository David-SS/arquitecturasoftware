
<%@page import="entities.AppUser"%>
<% 
   AppUser sessionUserHeader = (AppUser)session.getAttribute("user");
%>

<header>
    <img src="Images/ForWebPage/Wallpaper_3.jpg" width="100%" height="350">
</header>    
<nav class="navbar navbar-expand-sm bg-mine navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=HomePage">Inicio</a></li>
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=RecipeList">Mis Recetas</a></li>
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=FavoriteRecipeList">Recetas Favoritas</a></li>
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=RecipeCatalog">Cat�logo</a></li>
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=Log">Log</a></li>       
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=Statistics">Estadisticas</a></li>       
        <li class="nav-item active"><a class="nav-link" href="FrontController?command=UnknownCommand">Ayuda</a></li>       
        <% if (sessionUserHeader != null) { %>
            <div id="userLogged" class="dropdown">
                <a id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-bars userMenu" aria-hidden="true"></i>
                </a>
                <%= " " +sessionUserHeader.getName() + " " + sessionUserHeader.getSurname() + " " %>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="FrontController?command=AccessModificateProfile">Editar perfil</a>
                  <a class="dropdown-item" href="FrontController?command=RemoveUser" onclick="return confirm('Va a borrar su perfil. �Esta Seguro?')">Borrar Perfil</a>
                  <a class="dropdown-item" href="FrontController?command=Logout">Cerrar Session</a>
                </div>
            </div>                
        <% } %>
        
    </ul>      
</nav
