
package command;

import ejb.model.singleton.LogSingleton;
import entities.AppUser;
import entities.Recipe;
import java.util.List;
import java.util.Objects;
import sessionBeanFacades.FavoriteRecipeFacade;

public class FavoriteRecipeList extends FrontCommand{
    
    private LogSingleton ls;
    private FavoriteRecipeFacade favoriteRecipeFacade;
    
    @Override
    public void process() {
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> FAVORITE RECIPE LIST COMMAND");
        
        favoriteRecipeFacade = getFavoriteRecipeFacade();
        
        checkFavoriteRecipesExist();
        forward("/JSP-Pages/FavoriteRecipeList.jsp");
    }

    private void checkFavoriteRecipesExist() {
        AppUser myUser = (AppUser)session.getAttribute("user");
        List<Recipe> favoriteRecipes = favoriteRecipeFacade.getAllUserFavoriteRecipes(myUser);
        if (favoriteRecipes != null)
            for (Recipe favoriteRecipe : favoriteRecipes)
                if (favoriteRecipe.getPrivate1() == 1 && !Objects.equals(favoriteRecipe.getUserId().getId(), myUser.getId())) 
                    favoriteRecipeFacade.deleteFavoriteRecipeById(favoriteRecipe, myUser);
    }
}
