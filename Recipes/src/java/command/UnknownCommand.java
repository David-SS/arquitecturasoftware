
package command;

import ejb.model.singleton.LogSingleton;

public class UnknownCommand extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {
        ls = getLogBean();
        ls.addLogMessage("--> **UNKNOWN COMMAND**");
        forward("/JSP-Pages/UnknownCommand.jsp");
    }
}
