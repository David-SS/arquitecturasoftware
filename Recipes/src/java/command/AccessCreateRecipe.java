
package command;

import ejb.model.singleton.LogSingleton;

public class AccessCreateRecipe extends FrontCommand{
    
    private LogSingleton ls;
    
    @Override
    public void process() {        
        ls = getLogBean();      
        ls.addLogMessage("USUARIO: " + getSessionUsername() + " --> ACCESS CREATE RECIPES COMMAND");
        forward("/JSP-Pages/CreateRecipe.jsp");
    }
}
