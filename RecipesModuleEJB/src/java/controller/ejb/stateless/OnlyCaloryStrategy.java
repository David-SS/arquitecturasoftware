
package controller.ejb.stateless;

import ejb.model.singleton.LogSingleton;
import entities.Ingredient;
import entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import strategy.FilterAndOrderStrategy;

@Stateless
@LocalBean
public class OnlyCaloryStrategy extends FilterAndOrderStrategy {

    private final int calories;
    private LogSingleton ls;
    
    public OnlyCaloryStrategy() {
//        try {
//            this.ls = (LogSingleton) InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
//        } catch (NamingException ex) {
//            Logger.getLogger(OnlyCaloryStrategy.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        ls.addLogMessage("OnlyCaloryStrategy:OnlyCaloryStrategy");
        this.calories = -1;
    }
    
    public OnlyCaloryStrategy(int calories) {
//        try {
//            this.ls = (LogSingleton) InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
//        } catch (NamingException ex) {
//            Logger.getLogger(OnlyCaloryStrategy.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        ls.addLogMessage("OnlyCaloryStrategy:OnlyCaloryStrategy");
        this.calories = calories;
    }
    
    @Override
    public List<Recipe> getFilteredResult(List<Recipe> recipeCatalog) {
//        ls.addLogMessage("OnlyCaloryStrategy:getFilteredResult");
        List<Recipe> filteredRecipes = new ArrayList<>();
        for (Recipe recipe : recipeCatalog) {
            if (calculateCalories(recipe)) filteredRecipes.add(recipe);
        }
        return filteredRecipes;
    }

    private boolean calculateCalories(Recipe recipe) {
//        ls.addLogMessage("OnlyCaloryStrategy:calculateCalories");
        int caloriesCount = 0;
        for (Ingredient ingredient : recipe.getIngredientCollection()) {
            caloriesCount += Integer.parseInt(ingredient.getCalories());
        }
        return (caloriesCount <= this.calories);
    }
        
    @PostConstruct
    void postConstruct() {
//        ls.addLogMessage("OnlyCaloryStrategy:POSTCONTRUCT");
    }    
    @PreDestroy
    void preDestroy() {
//        ls.addLogMessage("OnlyCaloryStrategy:PREDESTROY");
    }   

}
