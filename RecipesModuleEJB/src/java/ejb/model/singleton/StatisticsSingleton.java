
package ejb.model.singleton;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;

@Singleton
@LocalBean
public class StatisticsSingleton {

    private int userCount;
    private final Map<String,Integer> statisticsUser;
    private final Map<String,Integer> statisticsPage;
    
    public StatisticsSingleton(){
        this.userCount = 0;
        this.statisticsUser = new HashMap<>();
        this.statisticsPage = new HashMap<>();
    }
    
    @Lock(LockType.WRITE)
    public void addUser() {
        this.userCount++;
    }
    
    @Lock(LockType.WRITE)
    public void removeUser() {
        this.userCount--;
    }
    
    @Lock(LockType.WRITE)
    public void addLogMessage(String message) {
        Integer concreteStatistic = statisticsUser.get(message);
        if (concreteStatistic == null)
            concreteStatistic = 0;
        concreteStatistic++;
        statisticsUser.put(message, concreteStatistic);
    }
    
    @Lock(LockType.WRITE)
    public void addLogMessageForPage(String message) {
        Integer concreteStatistic = statisticsPage.get(message);
        if (concreteStatistic == null)
            concreteStatistic = 0;
        concreteStatistic++;
        statisticsPage.put(message, concreteStatistic);
    }
    
    @Lock(LockType.READ)
    public int getLogedUser(){
        return this.userCount;
    }
    
    @Lock(LockType.READ)
    public Map<String, Integer> getStatistics() {
        return this.statisticsUser;
    }
    
    @Lock(LockType.READ)
    public Map<String, Integer> getPageStatistics() {
        return this.statisticsPage;
    }
}
