
package command;

import ejb.model.singleton.StatisticsSingleton;
import ejb.model.singleton.LogSingleton;
import encryptmd5.MD5;
import entities.AppUser;
import sessionBeanFacades.AppUserFacade;

public class Login extends FrontCommand{
    
    private LogSingleton ls;
    private StatisticsSingleton statistics;
    private AppUserFacade userFacade;
    
    @Override
    public void process() {    
        ls = getLogBean();        
        ls.addLogMessage("--> LOGIN COMMAND");
        statistics = getStatisticsBean();
        statistics.addUser();
        
        userFacade = getUserFacade();        
        if (checkLogin()) 
            forward("/JSP-Pages/HomePage.jsp");
        else 
            forward("/index.jsp");
    }
    
    private boolean checkLogin() {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        AppUser loginUser = userFacade.getLogedUser(username);
        if (loginUser != null && (MD5.encrypt(password)).equals(loginUser.getPassword())) {
            session.setAttribute("user", loginUser);
            return true;
        } 
        if (loginUser == null)
            System.out.println("No existe el usuario");
        return false;
    }
}
