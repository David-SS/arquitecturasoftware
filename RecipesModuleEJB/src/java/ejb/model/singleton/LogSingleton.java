
package ejb.model.singleton;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;

@Singleton
@LocalBean
public class LogSingleton {
    
    private final List<String> logMessages;
    
    public LogSingleton(){
        this.logMessages = new ArrayList<>();
    }
    
    @Lock(LockType.WRITE)
    public void addLogMessage(String message) {
        logMessages.add(message);
    }
    
    @Lock(LockType.READ)
    public List<String> getLogMessages() {
        return this.logMessages;
    }
    
}
