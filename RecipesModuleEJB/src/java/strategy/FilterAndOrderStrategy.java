
package strategy;

import entities.Recipe;
import java.util.List;

public abstract class FilterAndOrderStrategy {
    public abstract List<Recipe> getFilteredResult(List<Recipe> recipeCatalog);
}
