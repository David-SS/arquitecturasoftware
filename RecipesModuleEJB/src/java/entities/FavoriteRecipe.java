
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "favorite_recipe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FavoriteRecipe.findAll", query = "SELECT f FROM FavoriteRecipe f")
    , @NamedQuery(name = "FavoriteRecipe.findById", query = "SELECT f FROM FavoriteRecipe f WHERE f.id = :id")
    , @NamedQuery(name = "FavoriteRecipe.findAllFavoriteRecipes", query = "SELECT f FROM FavoriteRecipe f WHERE f.userId = :userId")
    , @NamedQuery(name = "FavoriteRecipe.findFavoriteRecipe", query = "SELECT f FROM FavoriteRecipe f WHERE f.userId = :userId AND f.recipeId = :recipeId")})
public class FavoriteRecipe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AppUser userId;
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Recipe recipeId;

    public FavoriteRecipe() {
    }

    public FavoriteRecipe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AppUser getUserId() {
        return userId;
    }

    public void setUserId(AppUser userId) {
        this.userId = userId;
    }

    public Recipe getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Recipe recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FavoriteRecipe)) {
            return false;
        }
        FavoriteRecipe other = (FavoriteRecipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.FavoriteRecipe[ id=" + id + " ]";
    }
    
}
