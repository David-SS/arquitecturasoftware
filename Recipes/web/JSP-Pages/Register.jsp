
<%@page import="ejb.model.singleton.StatisticsSingleton"%>
<%@page import="ejb.model.singleton.LogSingleton"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LogSingleton ls = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/LogSingleton!ejb.model.singleton.LogSingleton");
    ls.addLogMessage("--> JSP REGISTER");
    StatisticsSingleton statistics = InitialContext.doLookup("java:global/RecipeProjectAS/RecipesModuleEJB/StatisticsSingleton!ejb.model.singleton.StatisticsSingleton");
    statistics.addLogMessageForPage("Register.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="Includes/HeadConfig.jsp" %>
        <title> Registro </title>
    </head>
    <body>
        
        <%@include file="Includes/Header.jsp" %>
        
        <main>
            <h2 class="ActualPage">
                Registro
            </h2>
            <div class="contentMargin">        
                <div class="row">                    
                    <div class="inside_elements_margin">
                        <img id="inputImageItem" src="Images/user.png" width="600" height="auto">
                        <div class="form-group">
                            <input id="inputFileImage" name="inputFileImage" type="file" onchange="getImagePath();">
                        </div>
                    </div>
                    
                    <form action="FrontController">
                    <div class="inside_elements_margin">
                        <div class="form-group">
                            <label for="pwd"> Nombre: </label> 
                            <input placeholder="Nombre" class="form-control" type="text" name="name" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="pwd"> Apellidos: </label> 
                            <input placeholder="Apellido1 Apellido2" class="form-control" type="text" name="surname" required>
                        </div>     

                        <div class="form-group">
                            <label for="pwd"> Email: </label> 
                            <input placeholder="alguien@dominio.es" class="form-control" type="text" name="email" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Nombre de usuario: </label> 
                            <input placeholder="ReceMaker143" class="form-control" type="text" name="username" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Contraseña: </label> 
                            <input class="form-control" type="password" name="password" required>
                        </div>     
                        
                        <div class="form-group">
                            <label for="pwd"> Confirmación contraseña: </label> 
                            <input class="form-control" type="password" name="passwordConfirm" required>
                        </div>     

                        <input type="hidden" name="command" value="Register">
                        <input type="submit" class="btn btn-primary" value="Registrarse">
                    </div>   
                    </form>                 
                </div>
            </div>
        </main>   
        
        <%@include file="Includes/Footer.jsp" %>
        
    </body>    
</html>


